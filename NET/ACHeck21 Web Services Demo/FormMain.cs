﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Web;
using System.Threading;

namespace ACHeck21_Web_Services_Demo {
    public partial class FormMain : Form {

        // Since native TIFF support does not exist in .NET, this DLL will convert a TIFF to a JPEG.
        [DllImport("Tiff2JPEG.dll")]
        private static extern int ConvertToFile(string tiffFn, string jpegFn, int width, int comprQ);

        public FormMain() {
            InitializeComponent();
            MicroHandler.GetInstance(this).InitializeLocalVariables();
            GlobalGatewayHandler.GetInstance(this).InitializeLocalVariables();
        }

        private void FormMain_Load(object sender, EventArgs e) {
            FormAuthenticate.GetInstance().Connected += new FormAuthenticate.ConnectedDel(auth_Connected);
            FormAuthenticate.GetInstance().ShowDialog();

            if (FormAuthenticate.GetInstance().IsConnected) {
                LogQueryStart("FindUserClients", "");                
                
                IGlobalGateway gateway = new GlobalGatewaySOAP(FormAuthenticate.GetInstance().Username, FormAuthenticate.GetInstance().Password);
                com.acheck21.gateway.FindUserClientsResult result = gateway.FindUserClients(FormAuthenticate.GetInstance().Username);

                LogQueryEnd("FindUserClients");

                if (result.Code != (int)Enums.ResponseCode.NO_ERROR) {
                    MessageBox.Show("FindUserClients method error: " + result.Message + "", "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Close();
                } else {
                    foreach (com.acheck21.gateway.ShortClientInfo client in result.Clients) {
                        m_cbClientsReturns.Items.Add(client.ClientID + " - " + client.ClientName);
                        m_cbClientsTrans.Items.Add(client.ClientID + " - " + client.ClientName);
                        m_cbCheckFrom.Items.Add(client.ClientID + " - " + client.ClientName);
                        m_cbMicroAccount.Items.Add(client.ClientID + " - " + client.ClientName);
                    }
                    m_cbClientsTrans.SelectedIndex = 0;
                    m_cbClientsReturns.SelectedIndex = 0;
                    m_cbCheckFrom.SelectedIndex = 0;
                    m_cbMicroAccount.SelectedIndex = 0;
                }
            } else
                Close();
        }

        private void auth_Connected(string username, string password) {
            FormAuthenticate.GetInstance().Close();
        }

        private void m_btnGetReturns_Click(object sender, EventArgs e) {
            GlobalGatewayHandler.GetInstance(this).BtnGetReturns_Click(sender, e);
        }

        private void m_btnGetTrans_Click(object sender, EventArgs e) {
            GlobalGatewayHandler.GetInstance(this).BtnGetTrans_Click(sender, e);
        }

        private void m_btnCreateCheck_Click(object sender, EventArgs e) {
            GlobalGatewayHandler.GetInstance(this).BtnCreateCheck_Click(sender, e);
        }

        /// <summary>
        /// Converts an image file into a usable Image object that is not locked while viewing
        /// </summary>
        /// <param name="fn">Path to valid image file</param>
        /// <returns>Image</returns>
        private Image GetImage(string fn) {
            // We have to read the image file into a memory stream or the image file itself will become
            // file locked while viewing the image in the PictureBox.
            using (FileStream fs = new FileStream(fn, FileMode.Open)) {
                BinaryReader reader = new BinaryReader(fs);
                MemoryStream ms = new MemoryStream(reader.ReadBytes((int)fs.Length));
                return Image.FromStream(fs);
            }
        }

        private void m_btnBrowseFrontImageGG_Click(object sender, EventArgs e) {
            GlobalGatewayHandler.GetInstance(this).BtnBrowseFrontImageGG_Click(sender, e);
        }

        private void m_btnBrowseRearImageGG_Click(object sender, EventArgs e) {
            GlobalGatewayHandler.GetInstance(this).BtnBrowseRearImageGG_Click(sender, e);
        }

        private void m_btnBrowseFrontImageMicro_Click(object sender, EventArgs e) {
            MicroHandler.GetInstance(this).BtnBrowseFrontImageMicro_Click(sender, e);
        }

        private void m_btnBrowseRearImageMicro_Click(object sender, EventArgs e) {
            MicroHandler.GetInstance(this).BtnBrowseRearImageMicro_Click(sender, e);
        }

        private string ParseClientId(string comboBoxValue) {
            string[] values = comboBoxValue.Split('-');
            return (values.Length > 1) ? values[0].Trim() : "";
        }

        private void m_btnUploadMicro_Click(object sender, EventArgs e) {
            MicroHandler.GetInstance(this).BtnUploadMicro_Click(sender, e);
        }

        private void m_BtnMicroDelete_Click(object sender, EventArgs e) {
            MicroHandler.GetInstance(this).BtnMicroDelete_Click(sender, e);
        }

        private void m_llMicroGateway_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            MicroHandler.GetInstance(this).LlMicroGateway_LinkClicked(sender, e);
        }

        private void m_btnCorrect_Click(object sender, EventArgs e) {
            MicroHandler.GetInstance(this).BtnCorrect_Click(sender, e);
        }

        private void m_btnChooseBatch_Click(object sender, EventArgs e) {
            if (m_openDlgBatch.ShowDialog() == DialogResult.OK) {
                m_tbBatchFile.Text = m_openDlgBatch.FileName;
                m_tbBatchContents.Lines = File.ReadAllLines(m_openDlgBatch.FileName);
            }
        }

        private void m_btnSendBatch_Click(object sender, EventArgs e) {
            GlobalGatewayHandler.GetInstance(this).BtnSendBatch_Click(sender, e);
        }

        private void LogQueryStart(string methodName, string query) {
            string q = query.Replace("\n", "\r\n");

            StringBuilder sb = new StringBuilder();
            sb.Append("**************************************************************\r\n");
            sb.Append("SERVICE CALL METHOD: ").Append(methodName).Append("\r\n");
            sb.Append("SERVICE CALL START: ").Append(DateTime.Now.ToShortDateString()).Append(" at ").Append(DateTime.Now.ToLongTimeString()).Append("\r\n");
            sb.Append("SERVICE CALL QUERY: \r\n").Append(q);
            m_tbQueryLog.Text += sb.ToString();
        }

        private void LogQueryEnd(string methodName) {
            StringBuilder sb = new StringBuilder();
            sb.Append("SERVICE CALL END: ").Append(DateTime.Now.ToShortDateString()).Append(" at ").Append(DateTime.Now.ToLongTimeString()).Append("\r\n");
            m_tbQueryLog.Text += sb.ToString();
        }

        /// <summary>
        /// This class is used as an encapsulation system for the Form to keep Global Gateway service code concise and well organized
        /// </summary>
        private partial class GlobalGatewayHandler {
            private static GlobalGatewayHandler _handler;
            private FormMain m_frm;

            public static GlobalGatewayHandler GetInstance(FormMain frm) {
                if (_handler == null)
                    _handler = new GlobalGatewayHandler(frm);

                return _handler;
            }

            private GlobalGatewayHandler(FormMain frm) {
                m_frm = frm;
            }
        }

        /// <summary>
        /// This class is used as an encapsulation system for the Form to keep Micro service code concise and well organized
        /// </summary>
        private partial class MicroHandler {
            private static MicroHandler _handler;
            private FormMain m_frm;

            public static MicroHandler GetInstance(FormMain frm) {
                if (_handler == null)
                    _handler = new MicroHandler(frm);

                return _handler;
            }

            private MicroHandler(FormMain frm) {
                m_frm = frm;
            }
        }

    }
}
