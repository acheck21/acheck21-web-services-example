﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACHeck21_Web_Services_Demo {
    public class QueryGreaterThan : IQueryOperation {
        private string m_field;
        private string m_value;

        public QueryGreaterThan(string field, string value) {
            m_field = field;
            m_value = value;
        }

        #region IQueryOperation Members

        public string GenerateXML() {
            StringBuilder sb = new StringBuilder();
            sb.Append("<").Append(m_field).Append(">\n");
            sb.Append("\t<Operation>GreaterThan</Operation>\n");
            sb.Append("\t<Value>").Append(m_value).Append("</Value>\n");
            sb.Append("</").Append(m_field).Append(">\n");
            return sb.ToString();
        }

        string IQueryOperation.GenerateXML() {
            return GenerateXML();
        }

        public string GenerateGET() {
            StringBuilder sb = new StringBuilder();
            sb.Append(m_field).Append("=greaterthan,").Append(m_value);
            return sb.ToString();
        }

        string IQueryOperation.GenerateGET() {
            return GenerateGET();
        }

        #endregion

    }
}
