﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ACHeck21_Web_Services_Demo {
    public partial class FormAuthenticate : Form {
        public delegate void ConnectedDel(string username, string password);
        public event ConnectedDel Connected;

        private static FormAuthenticate _instance;

        private bool m_connected;

        public static FormAuthenticate GetInstance() {
            if (_instance == null)
                _instance = new FormAuthenticate();

            return _instance;
        }
        
        public FormAuthenticate() {
            InitializeComponent();

            m_connected = false;
        }

        private void m_btnConnect_Click(object sender, EventArgs e) {
            IGlobalGateway gateway = new GlobalGatewaySOAP(m_tbUsername.Text.Trim(), m_tbPassword.Text.Trim());
            com.acheck21.gateway.AuthenticateResult result = gateway.Authenticate();

            if (result.Code != (int)Enums.ResponseCode.NO_ERROR) {
                m_lblMessage.ForeColor = Color.Red;
                m_lblMessage.Text = result.Message;

                m_connected = false;
            } else {
                m_lblMessage.ForeColor = Color.Black;
                m_lblMessage.Text = "Connected Successfully";

                m_connected = true;

                if (Connected != null)
                    Connected(m_tbUsername.Text, m_tbPassword.Text);
            }     
        }

        private void FormAuthenticate_Load(object sender, EventArgs e) {
            m_lblMessage.Text = "";
        }

        public bool IsConnected {
            get { return m_connected; }
        }

        public string Username {
            get { return m_tbUsername.Text.Trim(); }
        }

        public string Password {
            get { return m_tbPassword.Text.Trim(); }
        }
    }
}
