﻿namespace ACHeck21_Web_Services_Demo {
    partial class FormAuthenticate {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.m_lblMessage = new System.Windows.Forms.Label();
            this.m_btnConnect = new System.Windows.Forms.Button();
            this.m_lblPassword = new System.Windows.Forms.Label();
            this.m_lblUsername = new System.Windows.Forms.Label();
            this.m_tbPassword = new System.Windows.Forms.TextBox();
            this.m_tbUsername = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // m_lblMessage
            // 
            this.m_lblMessage.AutoSize = true;
            this.m_lblMessage.ForeColor = System.Drawing.Color.Red;
            this.m_lblMessage.Location = new System.Drawing.Point(12, 150);
            this.m_lblMessage.Name = "m_lblMessage";
            this.m_lblMessage.Size = new System.Drawing.Size(83, 13);
            this.m_lblMessage.TabIndex = 11;
            this.m_lblMessage.Text = "Status Message";
            // 
            // m_btnConnect
            // 
            this.m_btnConnect.Location = new System.Drawing.Point(166, 111);
            this.m_btnConnect.Name = "m_btnConnect";
            this.m_btnConnect.Size = new System.Drawing.Size(89, 23);
            this.m_btnConnect.TabIndex = 10;
            this.m_btnConnect.Text = "Connect";
            this.m_btnConnect.UseVisualStyleBackColor = true;
            this.m_btnConnect.Click += new System.EventHandler(this.m_btnConnect_Click);
            // 
            // m_lblPassword
            // 
            this.m_lblPassword.AutoSize = true;
            this.m_lblPassword.Location = new System.Drawing.Point(12, 60);
            this.m_lblPassword.Name = "m_lblPassword";
            this.m_lblPassword.Size = new System.Drawing.Size(56, 13);
            this.m_lblPassword.TabIndex = 9;
            this.m_lblPassword.Text = "Password:";
            // 
            // m_lblUsername
            // 
            this.m_lblUsername.AutoSize = true;
            this.m_lblUsername.Location = new System.Drawing.Point(12, 9);
            this.m_lblUsername.Name = "m_lblUsername";
            this.m_lblUsername.Size = new System.Drawing.Size(58, 13);
            this.m_lblUsername.TabIndex = 8;
            this.m_lblUsername.Text = "Username:";
            // 
            // m_tbPassword
            // 
            this.m_tbPassword.Location = new System.Drawing.Point(15, 76);
            this.m_tbPassword.Name = "m_tbPassword";
            this.m_tbPassword.PasswordChar = '*';
            this.m_tbPassword.Size = new System.Drawing.Size(240, 20);
            this.m_tbPassword.TabIndex = 7;
            this.m_tbPassword.UseSystemPasswordChar = true;
            // 
            // m_tbUsername
            // 
            this.m_tbUsername.Location = new System.Drawing.Point(15, 25);
            this.m_tbUsername.Name = "m_tbUsername";
            this.m_tbUsername.Size = new System.Drawing.Size(240, 20);
            this.m_tbUsername.TabIndex = 6;
            // 
            // FormAuthenticate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(270, 175);
            this.Controls.Add(this.m_lblMessage);
            this.Controls.Add(this.m_btnConnect);
            this.Controls.Add(this.m_lblPassword);
            this.Controls.Add(this.m_lblUsername);
            this.Controls.Add(this.m_tbPassword);
            this.Controls.Add(this.m_tbUsername);
            this.Name = "FormAuthenticate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer Login";
            this.Load += new System.EventHandler(this.FormAuthenticate_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label m_lblMessage;
        private System.Windows.Forms.Button m_btnConnect;
        private System.Windows.Forms.Label m_lblPassword;
        private System.Windows.Forms.Label m_lblUsername;
        private System.Windows.Forms.TextBox m_tbPassword;
        private System.Windows.Forms.TextBox m_tbUsername;
    }
}