﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACHeck21_Web_Services_Demo {
    public interface IQueryOperation {
        string GenerateXML();
        string GenerateGET();
    }
}
