﻿using System;
namespace ACHeck21_Web_Services_Demo {
    interface IGlobalGateway {
        ACHeck21_Web_Services_Demo.com.acheck21.gateway.AuthenticateResult Authenticate();
        ACHeck21_Web_Services_Demo.com.acheck21.gateway.AuthorizeCheckResult AuthorizeCheck(string clientId, string micr, string routingNr, string dda, string checkNr, decimal amount, string dlNumber, string phone);
        ACHeck21_Web_Services_Demo.com.acheck21.gateway.CreateCheckResult CreateCheck(string clientId, string clientTag, string payee, string checkNr, string routingNr, string dda, decimal amount, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass ecc, string frontFn, string rearFn, string micr, DateTime postDate, string[] addenda);
        ACHeck21_Web_Services_Demo.com.acheck21.gateway.CreateCheckWithIRResult CreateCheckWithIR(string clientId, string payee, string checkNr, string routingNr, string dda, decimal amount, string frontFn, string rearFn, string micr, DateTime postDate);
        ACHeck21_Web_Services_Demo.com.acheck21.gateway.CreateValidatedCheckResult CreateValidatedCheck(string clientId, string clientTag, string payee, string checkNr, string routingNr, string dda, decimal amount, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass ecc, string frontFn, string rearFn, string micr, string dlNumber, string phone, DateTime postDate, string[] addenda);
        ACHeck21_Web_Services_Demo.com.acheck21.gateway.CreateValidatedCheckWithIRResult CreateValidatedCheckWithIR(string clientId, string payee, string checkNr, string routingNr, string dda, decimal amount, string frontFn, string rearFn, string micr, string dlNumber, string phone, DateTime postDate);
        ACHeck21_Web_Services_Demo.com.acheck21.gateway.FindChecksDetailsResult FindChecksDetails(string clientId, string query);
        ACHeck21_Web_Services_Demo.com.acheck21.gateway.FindReturnsDetailsResult FindReturnsDetails(string clientId, string query);
        ACHeck21_Web_Services_Demo.com.acheck21.gateway.FindUserClientsResult FindUserClients(string username);
        ACHeck21_Web_Services_Demo.com.acheck21.gateway.SendBatchResults SendBatch(string clientId, string filename);
        ACHeck21_Web_Services_Demo.com.acheck21.gateway.CreateRCCResult CreateRCC(string clientId, string payee, string checkNr, string routingNr, string dda, decimal amount, DateTime checkDate, string payer, string addr1, string addr2, string addr3, string endorse1, string endorse2, string endorse3, string endorse4, string sigText);
    }
}
