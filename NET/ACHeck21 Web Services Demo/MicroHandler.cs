﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;

namespace ACHeck21_Web_Services_Demo {
    public partial class FormMain {

        private partial class MicroHandler {
            private string m_microFrontFn, m_microRearFn;

            private enum MicroStatus { WAITING, PROCESSING, GOOD, BAD };

            public void InitializeLocalVariables() {

                SetMicroStatus(MicroStatus.WAITING);

                m_frm.m_llMicroGateway.Text = "https://gateway.acheck21.com/Micro";
            }

            private void SetMicroStatus(MicroStatus status) {
                SetMicroStatus(status, null);
            }

            private void SetMicroStatus(MicroStatus status, com.acheck21.gateway.micro.PendingItem item) {
                switch (status) {
                    case MicroStatus.WAITING: {
                            m_frm.m_gbMicroCheck.Enabled = false;
                            m_frm.m_pnlMicroCheck.Enabled = true;
                            m_frm.m_osStatus.BackColor = Color.White;

                            m_frm.m_tbRoutingNrMicro.BackColor = Color.White;
                            m_frm.m_tbAccountNrMicro.BackColor = Color.White;
                            m_frm.m_tbCheckNrMicro.BackColor = Color.White;
                            m_frm.m_tbCarLarAmount.BackColor = Color.White;
                            m_frm.m_tbMergedMICR.BackColor = Color.White;

                            m_frm.m_tbMergedMICR.Text = "";
                            m_frm.m_cbMICR_OK.Checked = false;
                            m_frm.m_cbDuplicate.Checked = false;
                            m_frm.m_cbIQA.Checked = false;
                            m_frm.m_tbCheckNrMicro.Text = "";
                            m_frm.m_tbRoutingNrMicro.Text = "";
                            m_frm.m_tbAccountNrMicro.Text = "";
                            m_frm.m_tbCarLarAmount.Text = "0";
                            m_frm.m_lblRecordNumber.Text = "0";
                            break;
                        }
                    case MicroStatus.PROCESSING: {
                            m_frm.m_gbMicroCheck.Enabled = false;
                            m_frm.m_pnlMicroCheck.Enabled = false;
                            m_frm.m_osStatus.BackColor = Color.Yellow;

                            m_frm.m_tbRoutingNrMicro.BackColor = Color.White;
                            m_frm.m_tbAccountNrMicro.BackColor = Color.White;
                            m_frm.m_tbCheckNrMicro.BackColor = Color.White;
                            m_frm.m_tbCarLarAmount.BackColor = Color.White;
                            m_frm.m_tbMergedMICR.BackColor = Color.White;
                            break;
                        }
                    case MicroStatus.GOOD: {
                            m_frm.m_gbMicroCheck.Enabled = true;
                            m_frm.m_pnlMicroCheck.Enabled = false;
                            m_frm.m_osStatus.BackColor = Color.Green;

                            m_frm.m_tbRoutingNrMicro.BackColor = Color.White;
                            m_frm.m_tbAccountNrMicro.BackColor = Color.White;
                            m_frm.m_tbCheckNrMicro.BackColor = Color.White;
                            m_frm.m_tbCarLarAmount.BackColor = Color.White;
                            m_frm.m_tbMergedMICR.BackColor = Color.White;

                            if (item != null) {
                                m_frm.m_tbMergedMICR.Text = item.MICRLine.Trim();
                                m_frm.m_cbMICR_OK.Checked = item.MICR_OK;
                                m_frm.m_cbDuplicate.Checked = item.IsDuplicate;
                                m_frm.m_cbIQA.Checked = item.IQAErrors.Length == 0;
                                m_frm.m_tbCheckNrMicro.Text = item.Parsed_CheckNbr.Trim();
                                m_frm.m_tbRoutingNrMicro.Text = item.Parsed_RTN.Trim();
                                m_frm.m_tbAccountNrMicro.Text = item.Parsed_DDA.Trim();
                                m_frm.m_tbCarLarAmount.Text = item.Amount.ToString();
                                m_frm.m_lblRecordNumber.Text = item.RecordNumber.ToString();
                            } else {
                                    m_frm.m_tbRoutingNrMicro.BackColor = Color.Pink;
                                    m_frm.m_tbAccountNrMicro.BackColor = Color.Pink;
                                    m_frm.m_tbCheckNrMicro.BackColor = Color.Pink;
                                    m_frm.m_tbCarLarAmount.BackColor = Color.Pink;
                                    m_frm.m_tbMergedMICR.BackColor = Color.Pink;
                            }
                            break;
                        }
                    case MicroStatus.BAD: {
                            m_frm.m_gbMicroCheck.Enabled = true;
                            m_frm.m_pnlMicroCheck.Enabled = false;
                            m_frm.m_osStatus.BackColor = Color.Red;

                            if (item != null) {
                                if (item.Parsed_RTN.Trim().Length == 0)
                                    m_frm.m_tbRoutingNrMicro.BackColor = Color.Pink;
                                if (item.Parsed_DDA.Trim().Length == 0)
                                    m_frm.m_tbAccountNrMicro.BackColor = Color.Pink;
                                if (item.Parsed_CheckNbr.Trim().Length == 0)
                                    m_frm.m_tbCheckNrMicro.BackColor = Color.Pink;
                                if (item.Amount == 0)
                                    m_frm.m_tbCarLarAmount.BackColor = Color.Pink;
                                if (!item.MICR_OK)
                                    m_frm.m_tbMergedMICR.BackColor = Color.Pink;

                                m_frm.m_tbMergedMICR.Text = item.MICRLine.Trim();
                                m_frm.m_cbMICR_OK.Checked = item.MICR_OK;
                                m_frm.m_cbDuplicate.Checked = item.IsDuplicate;
                                m_frm.m_cbIQA.Checked = item.IQAErrors.Length == 0;
                                m_frm.m_tbCheckNrMicro.Text = item.Parsed_CheckNbr.Trim();
                                m_frm.m_tbRoutingNrMicro.Text = item.Parsed_RTN.Trim();
                                m_frm.m_tbAccountNrMicro.Text = item.Parsed_DDA.Trim();
                                m_frm.m_tbCarLarAmount.Text = item.Amount.ToString();
                                m_frm.m_lblRecordNumber.Text = item.RecordNumber.ToString();
                            } else {
                                    m_frm.m_tbRoutingNrMicro.BackColor = Color.Pink;
                                    m_frm.m_tbAccountNrMicro.BackColor = Color.Pink;
                                    m_frm.m_tbCheckNrMicro.BackColor = Color.Pink;
                                    m_frm.m_tbCarLarAmount.BackColor = Color.Pink;
                                    m_frm.m_tbMergedMICR.BackColor = Color.Pink;
                            }
                            break;
                        }
                }
            }

            public void BtnUploadMicro_Click(object sender, EventArgs e) {
                if (FormAuthenticate.GetInstance().IsConnected) {
                    SetMicroStatus(MicroStatus.PROCESSING);

                    // Update the UI
                    Application.DoEvents();

                    MicroGatewaySOAP micro = new MicroGatewaySOAP(FormAuthenticate.GetInstance().Username, FormAuthenticate.GetInstance().Password);

                    StringBuilder str = new StringBuilder();

                    m_frm.LogQueryStart("Authenticate", "");
                    com.acheck21.gateway.micro.AuthenticateResult authResult = micro.Authenticate();
                    m_frm.LogQueryEnd("Authenticate");

                    // Update the UI
                    Application.DoEvents();

                    if (authResult.Code != (int)Enums.ResponseCode.NO_ERROR) {
                        SetMicroStatus(MicroStatus.WAITING);
                        MessageBox.Show("Authenticate method error: " + authResult.Message + "", "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    } else {
                        m_frm.m_llMicroGateway.Text = "https://gateway.acheck21.com/Micro/Login.aspx?sid=" + MicroGatewaySOAP.SessionKey + "&&cid=" + m_frm.ParseClientId(m_frm.m_cbCheckFrom.Text);

                        m_frm.LogQueryStart("UploadCheckImage", "");
                        com.acheck21.gateway.micro.UploadCheckImageResult uploadResult = micro.UploadCheckImage(m_frm.ParseClientId(m_frm.m_cbCheckFrom.Text),
                            m_frm.m_tbHwMICR.Text.Trim(), m_microFrontFn, m_microRearFn);
                        m_frm.LogQueryEnd("UploadCheckImage");

                        // Update the UI
                        Application.DoEvents();

                        if (uploadResult.Code != (int)Enums.ResponseCode.NO_ERROR) {
                            MessageBox.Show("UploadCheckImage method error: " + uploadResult.Message + "", "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        } else {
                            int recordNr = uploadResult.RecordNumber;

                            m_frm.LogQueryStart("GetPendingItemsResult", "");
                            com.acheck21.gateway.micro.GetPendingItemsResult itemsResult = new ACHeck21_Web_Services_Demo.com.acheck21.gateway.micro.GetPendingItemsResult();

                            bool processing = true;
                            int retry = 30;

                            // Keep testing the check until processing has been completed and then display the results
                            while (processing && retry >= 0) {
                                itemsResult = micro.GetPendingItems(m_frm.ParseClientId(m_frm.m_cbCheckFrom.Text));

                                var items = from p in itemsResult.Items
                                            where p.RecordNumber == recordNr
                                            select p;

                                com.acheck21.gateway.micro.PendingItem item = items.FirstOrDefault<com.acheck21.gateway.micro.PendingItem>();
                                if (item.IsProcessing) {
                                    SetMicroStatus(MicroStatus.PROCESSING);

                                    // Update the UI
                                    Application.DoEvents();
                                    Thread.Sleep(1000);

                                    retry--;
                                } else {
                                    if (item.MICR_OK && !item.IsDuplicate && item.IQAErrors.Length == 0
                                        && item.Parsed_CheckNbr.Length > 0 && item.Parsed_DDA.Length > 0
                                        && item.Parsed_RTN.Length > 0 && item.Amount > 0) {
                                        SetMicroStatus(MicroStatus.GOOD, item);
                                    } else {
                                        SetMicroStatus(MicroStatus.BAD, item);
                                    }
                                    break;
                                }

                                if (retry == 0) {
                                    SetMicroStatus(MicroStatus.WAITING);
                                    MessageBox.Show("GetPendingItemsResult method error: processing took longer than 30 seconds", "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                }
                            }
                            m_frm.LogQueryEnd("GetPendingItemsResult");
                        }
                    }
                }
            }

            public void BtnMicroDelete_Click(object sender, EventArgs e) {
                if (FormAuthenticate.GetInstance().IsConnected) {
                    SetMicroStatus(MicroStatus.PROCESSING);

                    // Update the UI
                    Application.DoEvents();

                    MicroGatewaySOAP micro = new MicroGatewaySOAP(FormAuthenticate.GetInstance().Username, FormAuthenticate.GetInstance().Password);

                    StringBuilder str = new StringBuilder();

                    m_frm.LogQueryStart("Authenticate", "");
                    com.acheck21.gateway.micro.AuthenticateResult authResult = micro.Authenticate();
                    m_frm.LogQueryEnd("Authenticate");

                    // Update the UI
                    Application.DoEvents();

                    if (authResult.Code != (int)Enums.ResponseCode.NO_ERROR) {
                        MessageBox.Show("Authenticate method error: " + authResult.Message, "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    } else {
                        if (Convert.ToInt32(m_frm.m_lblRecordNumber.Text.Trim()) > 0) {
                            m_frm.LogQueryStart("DeletePendingCheck", "");
                            com.acheck21.gateway.micro.DeletePendingCheckResult result = micro.DeletePendingCheck(Convert.ToInt32(m_frm.m_lblRecordNumber.Text.Trim()));
                            m_frm.LogQueryEnd("DeletePendingCheck");

                            if (result.Code != (int)Enums.ResponseCode.NO_ERROR) {
                                SetMicroStatus(MicroStatus.WAITING);
                                MessageBox.Show("DeletePendingCheck method error: " + result.Message, "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            } else {
                                SetMicroStatus(MicroStatus.WAITING);
                                MessageBox.Show("Check Deleted Successfully", "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        } else {
                            SetMicroStatus(MicroStatus.WAITING);
                            MessageBox.Show("DeletePendingCheck method error: nothing to delete", "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
            }

            public void BtnCorrect_Click(object sender, EventArgs e) {
                if (FormAuthenticate.GetInstance().IsConnected) {
                    SetMicroStatus(MicroStatus.PROCESSING);

                    // Update the UI
                    Application.DoEvents();

                    MicroGatewaySOAP micro = new MicroGatewaySOAP(FormAuthenticate.GetInstance().Username, FormAuthenticate.GetInstance().Password);

                    m_frm.LogQueryStart("Authenticate", "");
                    com.acheck21.gateway.micro.AuthenticateResult authResult = micro.Authenticate();
                    m_frm.LogQueryEnd("Authenticate");

                    // Update the UI
                    Application.DoEvents();

                    if (authResult.Code != (int)Enums.ResponseCode.NO_ERROR) {
                        MessageBox.Show("Authenticate method error: " + authResult.Message, "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    } else {
                        // First test the Record Number to be sure we have something to work with
                        if (Convert.ToInt32(m_frm.m_lblRecordNumber.Text.Trim()) > 0) {
                            m_frm.LogQueryStart("UpdatePendingCheck", "");
                            com.acheck21.gateway.micro.UpdatePendingCheckResult result = micro.UpdatePendingCheck(Convert.ToInt32(m_frm.m_lblRecordNumber.Text.Trim()), 
                                m_frm.m_tbRoutingNrMicro.Text.Trim(), m_frm.m_tbAccountNrMicro.Text.Trim(), m_frm.m_tbCheckNrMicro.Text.Trim(),
                                Convert.ToDecimal(m_frm.m_tbCarLarAmount.Text.Trim()), true);
                            m_frm.LogQueryEnd("UpdatePendingCheck");

                            if (result.Code != (int)Enums.ResponseCode.NO_ERROR) {
                                SetMicroStatus(MicroStatus.BAD);
                                MessageBox.Show("UpdatePendingCheck method error: " + result.Message, "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            } else {
                                int recordNr = Convert.ToInt32(m_frm.m_lblRecordNumber.Text.Trim());

                                // Update the UI
                                Application.DoEvents();

                                m_frm.LogQueryStart("GetPendingItemsResult", "");
                                com.acheck21.gateway.micro.GetPendingItemsResult itemsResult = new ACHeck21_Web_Services_Demo.com.acheck21.gateway.micro.GetPendingItemsResult();

                                bool processing = true;
                                int retry = 30;

                                // Keep testing the check until processing has been completed and then display the results
                                while (processing && retry >= 0) {
                                    itemsResult = micro.GetPendingItems(m_frm.ParseClientId(m_frm.m_cbCheckFrom.Text));

                                    var items = from p in itemsResult.Items
                                                where p.RecordNumber == recordNr
                                                select p;

                                    com.acheck21.gateway.micro.PendingItem item = items.FirstOrDefault<com.acheck21.gateway.micro.PendingItem>();
                                    if (item.IsProcessing) {
                                        SetMicroStatus(MicroStatus.PROCESSING);

                                        Application.DoEvents();
                                        Thread.Sleep(1000);

                                        retry--;
                                    } else {
                                        if (item.MICR_OK && !item.IsDuplicate && item.IQAErrors.Length == 0
                                            && item.Parsed_CheckNbr.Length > 0 && item.Parsed_DDA.Length > 0
                                            && item.Parsed_RTN.Length > 0 && item.Amount > 0) {
                                            SetMicroStatus(MicroStatus.GOOD, item);
                                        } else {
                                            SetMicroStatus(MicroStatus.BAD, item);
                                        }
                                        break;
                                    }

                                    if (retry == 0) {
                                        SetMicroStatus(MicroStatus.WAITING);
                                        MessageBox.Show("GetPendingItemsResult method error: processing took longer than 30 seconds", "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    }
                                }
                                m_frm.LogQueryEnd("GetPendingItemsResult");
                            }
                        } else {
                            SetMicroStatus(MicroStatus.WAITING);
                            MessageBox.Show("UpdatePendingCheck method error: nothing to update", "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
            }

            public void LlMicroGateway_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
                System.Diagnostics.Process.Start(m_frm.m_llMicroGateway.Text);
            }

            public void BtnBrowseFrontImageMicro_Click(object sender, EventArgs e) {
                if (m_frm.m_openDlgImage.ShowDialog() == DialogResult.OK) {
                    m_microFrontFn = m_frm.m_openDlgImage.FileName.Trim();
                    int size = ConvertToFile(m_microFrontFn, "frontImageMicro.jpg", 0, 50);
                    m_frm.m_pbFrontMicro.Image = m_frm.GetImage("frontImageMicro.jpg");
                }
            }

            public void BtnBrowseRearImageMicro_Click(object sender, EventArgs e) {
                if (m_frm.m_openDlgImage.ShowDialog() == DialogResult.OK) {
                    m_microRearFn = m_frm.m_openDlgImage.FileName.Trim();
                    int size = ConvertToFile(m_microRearFn, "rearImageMicro.jpg", 0, 50);
                    m_frm.m_pbRearMicro.Image = m_frm.GetImage("rearImageMicro.jpg");
                }
            }
        }
    }
}
