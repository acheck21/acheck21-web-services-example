﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ACHeck21_Web_Services_Demo {
    public class MicroGatewaySOAP {
        private static com.acheck21.gateway.micro.ACHeck21Microv21 _micro = new ACHeck21_Web_Services_Demo.com.acheck21.gateway.micro.ACHeck21Microv21();
        private string m_username = "", m_password = "";
        private static string _securityKey = "";

        private string EncodeImages(string imageFn) {
            byte[] imgData;
            if (File.Exists(imageFn)) {
                imgData = File.ReadAllBytes(imageFn);
                return Convert.ToBase64String(imgData);
            } else
                throw new FileNotFoundException("Unable to locate image file", imageFn);
        }

        public MicroGatewaySOAP(string username, string password) {
            m_username = username;
            m_password = password;
        }

        public com.acheck21.gateway.micro.AuthenticateResult Authenticate() {
            com.acheck21.gateway.micro.AuthenticateResult result = _micro.Authenticate(m_username, m_password);
            if (result.Code == (int)Enums.ResponseCode.NO_ERROR)
                _securityKey = result.Data;

            return result;
        }

        public com.acheck21.gateway.micro.UploadCheckImageResult UploadCheckImage(string clientId, string micr, string frontFn, string rearFn) {
            string front = "", rear = "";
            // Get Base64 encoded string of image data
            if (File.Exists(frontFn)) {
                front = EncodeImages(frontFn);
            }
            if (File.Exists(rearFn)) {
                rear = EncodeImages(rearFn);
            }

            return _micro.UploadCheckImage(_securityKey, clientId, micr, front, rear);
        }

        public com.acheck21.gateway.micro.DeletePendingCheckResult DeletePendingCheck(int recordNr) {
            return _micro.DeletePendingCheck(_securityKey, recordNr);
        }

        public com.acheck21.gateway.micro.GetPendingItemsResult GetPendingItems(string clientId) {
            return _micro.GetPendingItems(_securityKey, clientId);
        }

        public com.acheck21.gateway.micro.UpdatePendingCheckResult UpdatePendingCheck(int recordNr, string rtn, string dda, string checkNr,
            decimal? amt, bool? approveIQA) {
            return _micro.UpdatePendingCheck(_securityKey, recordNr, rtn, dda, checkNr, amt, approveIQA);
        }

        public static string SessionKey {
            get { return _securityKey; }
        }
    }
}
