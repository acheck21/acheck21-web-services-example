﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACHeck21_Web_Services_Demo {
    public static class QueryGenerator {
        private enum QueryType { XML, GET };

        public static string GenerateXML(List<IQueryOperation> queryList) {
            return Generate(queryList, QueryType.XML);
        }

        public static string GenerateGET(List<IQueryOperation> queryList) {
            return Generate(queryList, QueryType.GET);
        }

        private static string Generate(List<IQueryOperation> queryList, QueryType type) {
            StringBuilder sb = new StringBuilder();
            if (type == QueryType.GET)
                sb.Append("?");
            foreach (IQueryOperation query in queryList) {
                sb.Append((type == QueryType.XML) ? query.GenerateXML() : query.GenerateGET());
                if (type == QueryType.GET)
                    sb.Append("&");
            }

            return sb.ToString();
        }
    }
}
