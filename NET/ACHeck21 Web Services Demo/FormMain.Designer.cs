﻿namespace ACHeck21_Web_Services_Demo {
    partial class FormMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.m_dsReturns = new System.Data.DataSet();
            this.m_openDlgImage = new System.Windows.Forms.OpenFileDialog();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.m_tbQueryLog = new System.Windows.Forms.TextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.m_gbMicroCheck = new System.Windows.Forms.GroupBox();
            this.m_llMicroGateway = new System.Windows.Forms.LinkLabel();
            this.m_cbDuplicate = new System.Windows.Forms.CheckBox();
            this.m_btnMicroDelete = new System.Windows.Forms.Button();
            this.m_lblRecordNumber = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.m_btnCorrect = new System.Windows.Forms.Button();
            this.m_cbMICR_OK = new System.Windows.Forms.CheckBox();
            this.m_cbIQA = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.m_tbMergedMICR = new System.Windows.Forms.TextBox();
            this.m_tbCarLarAmount = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.m_tbCheckNrMicro = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.m_tbAccountNrMicro = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.m_tbRoutingNrMicro = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.m_osStatus = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.m_pnlMicroCheck = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.m_cbMicroAccount = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.m_tbHwMICR = new System.Windows.Forms.TextBox();
            this.m_btnUploadMicro = new System.Windows.Forms.Button();
            this.m_pbRearMicro = new System.Windows.Forms.PictureBox();
            this.m_pbFrontMicro = new System.Windows.Forms.PictureBox();
            this.m_btnBrowseRearImageMicro = new System.Windows.Forms.Button();
            this.m_btnBrowseFrontImageMicro = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.m_openDlgBatch = new System.Windows.Forms.OpenFileDialog();
            this.m_tabReturns = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.m_dtpReturnsFrom = new System.Windows.Forms.DateTimePicker();
            this.m_dtpReturnsTo = new System.Windows.Forms.DateTimePicker();
            this.m_lblReturnsFrom = new System.Windows.Forms.Label();
            this.m_lblReturnsTo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.m_cbClientsReturns = new System.Windows.Forms.ComboBox();
            this.m_btnGetReturns = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.m_dgvReturns = new System.Windows.Forms.DataGridView();
            this.m_tabTransactions = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_dtpTransFrom = new System.Windows.Forms.DateTimePicker();
            this.m_dtpTransTo = new System.Windows.Forms.DateTimePicker();
            this.m_lblTransFrom = new System.Windows.Forms.Label();
            this.m_lblTransTo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.m_cbClientsTrans = new System.Windows.Forms.ComboBox();
            this.m_btnGetTrans = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.m_dgvTrans = new System.Windows.Forms.DataGridView();
            this.m_tabSendBatch = new System.Windows.Forms.TabPage();
            this.m_tbBatchFile = new System.Windows.Forms.TextBox();
            this.m_btnChooseBatch = new System.Windows.Forms.Button();
            this.label34 = new System.Windows.Forms.Label();
            this.m_tbBatchContents = new System.Windows.Forms.TextBox();
            this.m_btnSendBatch = new System.Windows.Forms.Button();
            this.m_tabCreateCheck = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.m_btnCreateCheck = new System.Windows.Forms.Button();
            this.m_cbCheckFrom = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.m_tbRoutingNrGG = new System.Windows.Forms.TextBox();
            this.m_tbAccountNrGG = new System.Windows.Forms.TextBox();
            this.m_tbCheckNrGG = new System.Windows.Forms.TextBox();
            this.m_tbAmountGG = new System.Windows.Forms.TextBox();
            this.m_tbPayTo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.m_btnBrowseFrontImageGG = new System.Windows.Forms.Button();
            this.m_btnBrowseRearImageGG = new System.Windows.Forms.Button();
            this.m_pbFrontGG = new System.Windows.Forms.PictureBox();
            this.m_pbRearGG = new System.Windows.Forms.PictureBox();
            this.m_cmbxEntryClass = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.m_cmbxMethodTarget = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.m_tbMicrGG = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.m_gbLegend = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label26 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label27 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label28 = new System.Windows.Forms.Label();
            this.m_cmbxProtocol = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.m_tabControl = new System.Windows.Forms.TabControl();
            ((System.ComponentModel.ISupportInitialize)(this.m_dsReturns)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.m_gbMicroCheck.SuspendLayout();
            this.m_pnlMicroCheck.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pbRearMicro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pbFrontMicro)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.m_tabReturns.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvReturns)).BeginInit();
            this.m_tabTransactions.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvTrans)).BeginInit();
            this.m_tabSendBatch.SuspendLayout();
            this.m_tabCreateCheck.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_pbFrontGG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pbRearGG)).BeginInit();
            this.m_gbLegend.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.m_tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_dsReturns
            // 
            this.m_dsReturns.DataSetName = "NewDataSet";
            // 
            // m_openDlgImage
            // 
            this.m_openDlgImage.FileName = "image.tif";
            this.m_openDlgImage.Filter = "TIFF Images|*.tif|All Files|*.*";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel7);
            this.tabPage3.Controls.Add(this.panel8);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(773, 448);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "Returns";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 80);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(767, 365);
            this.panel7.TabIndex = 1;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label20);
            this.panel8.Controls.Add(this.label21);
            this.panel8.Controls.Add(this.label22);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(767, 77);
            this.panel8.TabIndex = 0;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(212, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(10, 13);
            this.label20.TabIndex = 9;
            this.label20.Text = "-";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(225, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(23, 13);
            this.label21.TabIndex = 8;
            this.label21.Text = "To:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(33, 13);
            this.label22.TabIndex = 7;
            this.label22.Text = "From:";
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(773, 448);
            this.tabPage4.TabIndex = 2;
            this.tabPage4.Text = "Query Monitor";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.m_tbQueryLog);
            this.tabPage7.Location = new System.Drawing.Point(4, 25);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(773, 551);
            this.tabPage7.TabIndex = 2;
            this.tabPage7.Text = "Query Log";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // m_tbQueryLog
            // 
            this.m_tbQueryLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_tbQueryLog.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.m_tbQueryLog.Location = new System.Drawing.Point(3, 3);
            this.m_tbQueryLog.Multiline = true;
            this.m_tbQueryLog.Name = "m_tbQueryLog";
            this.m_tbQueryLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.m_tbQueryLog.Size = new System.Drawing.Size(767, 545);
            this.m_tbQueryLog.TabIndex = 1;
            this.m_tbQueryLog.WordWrap = false;
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.White;
            this.tabPage6.Controls.Add(this.m_gbMicroCheck);
            this.tabPage6.Controls.Add(this.m_pnlMicroCheck);
            this.tabPage6.Location = new System.Drawing.Point(4, 25);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(773, 551);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "Micro Gateway Services";
            // 
            // m_gbMicroCheck
            // 
            this.m_gbMicroCheck.Controls.Add(this.m_llMicroGateway);
            this.m_gbMicroCheck.Controls.Add(this.m_cbDuplicate);
            this.m_gbMicroCheck.Controls.Add(this.m_btnMicroDelete);
            this.m_gbMicroCheck.Controls.Add(this.m_lblRecordNumber);
            this.m_gbMicroCheck.Controls.Add(this.label33);
            this.m_gbMicroCheck.Controls.Add(this.m_btnCorrect);
            this.m_gbMicroCheck.Controls.Add(this.m_cbMICR_OK);
            this.m_gbMicroCheck.Controls.Add(this.m_cbIQA);
            this.m_gbMicroCheck.Controls.Add(this.label16);
            this.m_gbMicroCheck.Controls.Add(this.m_tbMergedMICR);
            this.m_gbMicroCheck.Controls.Add(this.m_tbCarLarAmount);
            this.m_gbMicroCheck.Controls.Add(this.label31);
            this.m_gbMicroCheck.Controls.Add(this.label30);
            this.m_gbMicroCheck.Controls.Add(this.m_tbCheckNrMicro);
            this.m_gbMicroCheck.Controls.Add(this.label19);
            this.m_gbMicroCheck.Controls.Add(this.m_tbAccountNrMicro);
            this.m_gbMicroCheck.Controls.Add(this.label18);
            this.m_gbMicroCheck.Controls.Add(this.m_tbRoutingNrMicro);
            this.m_gbMicroCheck.Controls.Add(this.label17);
            this.m_gbMicroCheck.Controls.Add(this.shapeContainer1);
            this.m_gbMicroCheck.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_gbMicroCheck.Location = new System.Drawing.Point(3, 3);
            this.m_gbMicroCheck.Name = "m_gbMicroCheck";
            this.m_gbMicroCheck.Size = new System.Drawing.Size(767, 220);
            this.m_gbMicroCheck.TabIndex = 40;
            this.m_gbMicroCheck.TabStop = false;
            this.m_gbMicroCheck.Text = "ACHeck21 Micro Processing";
            // 
            // m_llMicroGateway
            // 
            this.m_llMicroGateway.AutoSize = true;
            this.m_llMicroGateway.Location = new System.Drawing.Point(11, 20);
            this.m_llMicroGateway.Name = "m_llMicroGateway";
            this.m_llMicroGateway.Size = new System.Drawing.Size(188, 13);
            this.m_llMicroGateway.TabIndex = 73;
            this.m_llMicroGateway.TabStop = true;
            this.m_llMicroGateway.Text = "https://gateway.acheck21.com/Micro";
            this.m_llMicroGateway.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.m_llMicroGateway_LinkClicked);
            // 
            // m_cbDuplicate
            // 
            this.m_cbDuplicate.AutoSize = true;
            this.m_cbDuplicate.Enabled = false;
            this.m_cbDuplicate.Location = new System.Drawing.Point(252, 64);
            this.m_cbDuplicate.Name = "m_cbDuplicate";
            this.m_cbDuplicate.Size = new System.Drawing.Size(71, 17);
            this.m_cbDuplicate.TabIndex = 56;
            this.m_cbDuplicate.Text = "Duplicate";
            this.m_cbDuplicate.UseVisualStyleBackColor = true;
            // 
            // m_btnMicroDelete
            // 
            this.m_btnMicroDelete.Location = new System.Drawing.Point(546, 182);
            this.m_btnMicroDelete.Name = "m_btnMicroDelete";
            this.m_btnMicroDelete.Size = new System.Drawing.Size(82, 23);
            this.m_btnMicroDelete.TabIndex = 55;
            this.m_btnMicroDelete.Text = "Delete";
            this.m_btnMicroDelete.UseVisualStyleBackColor = true;
            this.m_btnMicroDelete.Click += new System.EventHandler(this.m_BtnMicroDelete_Click);
            // 
            // m_lblRecordNumber
            // 
            this.m_lblRecordNumber.AutoSize = true;
            this.m_lblRecordNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_lblRecordNumber.Location = new System.Drawing.Point(137, 41);
            this.m_lblRecordNumber.Name = "m_lblRecordNumber";
            this.m_lblRecordNumber.Size = new System.Drawing.Size(18, 20);
            this.m_lblRecordNumber.TabIndex = 54;
            this.m_lblRecordNumber.Text = "0";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(10, 41);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(121, 20);
            this.label33.TabIndex = 53;
            this.label33.Text = "Record Number";
            // 
            // m_btnCorrect
            // 
            this.m_btnCorrect.Location = new System.Drawing.Point(634, 182);
            this.m_btnCorrect.Name = "m_btnCorrect";
            this.m_btnCorrect.Size = new System.Drawing.Size(112, 23);
            this.m_btnCorrect.TabIndex = 52;
            this.m_btnCorrect.Text = "Apply Corrections";
            this.m_btnCorrect.UseVisualStyleBackColor = true;
            this.m_btnCorrect.Click += new System.EventHandler(this.m_btnCorrect_Click);
            // 
            // m_cbMICR_OK
            // 
            this.m_cbMICR_OK.AutoSize = true;
            this.m_cbMICR_OK.Enabled = false;
            this.m_cbMICR_OK.Location = new System.Drawing.Point(41, 87);
            this.m_cbMICR_OK.Name = "m_cbMICR_OK";
            this.m_cbMICR_OK.Size = new System.Drawing.Size(145, 17);
            this.m_cbMICR_OK.TabIndex = 51;
            this.m_cbMICR_OK.Text = "MICR Parse Test Passed";
            this.m_cbMICR_OK.UseVisualStyleBackColor = true;
            // 
            // m_cbIQA
            // 
            this.m_cbIQA.AutoSize = true;
            this.m_cbIQA.Enabled = false;
            this.m_cbIQA.Location = new System.Drawing.Point(41, 64);
            this.m_cbIQA.Name = "m_cbIQA";
            this.m_cbIQA.Size = new System.Drawing.Size(196, 17);
            this.m_cbIQA.TabIndex = 50;
            this.m_cbIQA.Text = "Image Quality Analysis Passed (IQA)";
            this.m_cbIQA.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(38, 166);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 13);
            this.label16.TabIndex = 49;
            this.label16.Text = "Corrected MICR";
            // 
            // m_tbMergedMICR
            // 
            this.m_tbMergedMICR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_tbMergedMICR.Location = new System.Drawing.Point(41, 182);
            this.m_tbMergedMICR.Name = "m_tbMergedMICR";
            this.m_tbMergedMICR.ReadOnly = true;
            this.m_tbMergedMICR.Size = new System.Drawing.Size(427, 20);
            this.m_tbMergedMICR.TabIndex = 48;
            // 
            // m_tbCarLarAmount
            // 
            this.m_tbCarLarAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_tbCarLarAmount.Location = new System.Drawing.Point(546, 64);
            this.m_tbCarLarAmount.Name = "m_tbCarLarAmount";
            this.m_tbCarLarAmount.Size = new System.Drawing.Size(165, 29);
            this.m_tbCarLarAmount.TabIndex = 47;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(543, 48);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(100, 13);
            this.label31.TabIndex = 46;
            this.label31.Text = "CAR / LAR Amount";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(479, 115);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(114, 13);
            this.label30.TabIndex = 44;
            this.label30.Text = "Parsed Check Number";
            // 
            // m_tbCheckNrMicro
            // 
            this.m_tbCheckNrMicro.Location = new System.Drawing.Point(482, 131);
            this.m_tbCheckNrMicro.Name = "m_tbCheckNrMicro";
            this.m_tbCheckNrMicro.Size = new System.Drawing.Size(124, 20);
            this.m_tbCheckNrMicro.TabIndex = 43;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(226, 115);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(120, 13);
            this.label19.TabIndex = 42;
            this.label19.Text = "Parsed Routing Number";
            // 
            // m_tbAccountNrMicro
            // 
            this.m_tbAccountNrMicro.Location = new System.Drawing.Point(229, 131);
            this.m_tbAccountNrMicro.Name = "m_tbAccountNrMicro";
            this.m_tbAccountNrMicro.Size = new System.Drawing.Size(211, 20);
            this.m_tbAccountNrMicro.TabIndex = 41;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(38, 115);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(115, 13);
            this.label18.TabIndex = 40;
            this.label18.Text = "Parsed Transit Number";
            // 
            // m_tbRoutingNrMicro
            // 
            this.m_tbRoutingNrMicro.Location = new System.Drawing.Point(41, 131);
            this.m_tbRoutingNrMicro.Name = "m_tbRoutingNrMicro";
            this.m_tbRoutingNrMicro.Size = new System.Drawing.Size(162, 20);
            this.m_tbRoutingNrMicro.TabIndex = 39;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(691, 134);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(37, 13);
            this.label17.TabIndex = 38;
            this.label17.Text = "Status";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.m_osStatus});
            this.shapeContainer1.Size = new System.Drawing.Size(761, 201);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // m_osStatus
            // 
            this.m_osStatus.BackColor = System.Drawing.Color.Red;
            this.m_osStatus.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque;
            this.m_osStatus.Location = new System.Drawing.Point(727, 112);
            this.m_osStatus.Name = "m_osStatus";
            this.m_osStatus.Size = new System.Drawing.Size(23, 22);
            // 
            // m_pnlMicroCheck
            // 
            this.m_pnlMicroCheck.Controls.Add(this.label32);
            this.m_pnlMicroCheck.Controls.Add(this.m_cbMicroAccount);
            this.m_pnlMicroCheck.Controls.Add(this.groupBox1);
            this.m_pnlMicroCheck.Controls.Add(this.label15);
            this.m_pnlMicroCheck.Controls.Add(this.m_tbHwMICR);
            this.m_pnlMicroCheck.Controls.Add(this.m_btnUploadMicro);
            this.m_pnlMicroCheck.Controls.Add(this.m_pbRearMicro);
            this.m_pnlMicroCheck.Controls.Add(this.m_pbFrontMicro);
            this.m_pnlMicroCheck.Controls.Add(this.m_btnBrowseRearImageMicro);
            this.m_pnlMicroCheck.Controls.Add(this.m_btnBrowseFrontImageMicro);
            this.m_pnlMicroCheck.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.m_pnlMicroCheck.Location = new System.Drawing.Point(3, 223);
            this.m_pnlMicroCheck.Name = "m_pnlMicroCheck";
            this.m_pnlMicroCheck.Size = new System.Drawing.Size(767, 325);
            this.m_pnlMicroCheck.TabIndex = 39;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(452, 225);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(47, 13);
            this.label32.TabIndex = 71;
            this.label32.Text = "Account";
            // 
            // m_cbMicroAccount
            // 
            this.m_cbMicroAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_cbMicroAccount.FormattingEnabled = true;
            this.m_cbMicroAccount.Location = new System.Drawing.Point(455, 241);
            this.m_cbMicroAccount.Name = "m_cbMicroAccount";
            this.m_cbMicroAccount.Size = new System.Drawing.Size(217, 21);
            this.m_cbMicroAccount.TabIndex = 70;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.pictureBox9);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.pictureBox10);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.pictureBox11);
            this.groupBox1.Location = new System.Drawing.Point(14, 156);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(602, 51);
            this.groupBox1.TabIndex = 69;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MICR Symbol Conversion Legend";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(512, 20);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 17);
            this.label12.TabIndex = 35;
            this.label12.Text = "-       (dash)";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::ACHeck21_Web_Services_Demo.Properties.Resources.dash;
            this.pictureBox9.InitialImage = global::ACHeck21_Web_Services_Demo.Properties.Resources.transit;
            this.pictureBox9.Location = new System.Drawing.Point(458, 14);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(27, 29);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox9.TabIndex = 34;
            this.pictureBox9.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(284, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 17);
            this.label13.TabIndex = 36;
            this.label13.Text = "c       (on-us)";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::ACHeck21_Web_Services_Demo.Properties.Resources.onus;
            this.pictureBox10.InitialImage = global::ACHeck21_Web_Services_Demo.Properties.Resources.transit;
            this.pictureBox10.Location = new System.Drawing.Point(230, 16);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(27, 27);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox10.TabIndex = 33;
            this.pictureBox10.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(62, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(93, 17);
            this.label14.TabIndex = 32;
            this.label14.Text = "d       (transit)";
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::ACHeck21_Web_Services_Demo.Properties.Resources.transit;
            this.pictureBox11.InitialImage = global::ACHeck21_Web_Services_Demo.Properties.Resources.transit;
            this.pictureBox11.Location = new System.Drawing.Point(8, 16);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(27, 27);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox11.TabIndex = 31;
            this.pictureBox11.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 225);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(154, 13);
            this.label15.TabIndex = 68;
            this.label15.Text = "Hardware MICR Line (Optional)";
            // 
            // m_tbHwMICR
            // 
            this.m_tbHwMICR.Location = new System.Drawing.Point(14, 241);
            this.m_tbHwMICR.Name = "m_tbHwMICR";
            this.m_tbHwMICR.Size = new System.Drawing.Size(383, 20);
            this.m_tbHwMICR.TabIndex = 67;
            // 
            // m_btnUploadMicro
            // 
            this.m_btnUploadMicro.Location = new System.Drawing.Point(584, 273);
            this.m_btnUploadMicro.Name = "m_btnUploadMicro";
            this.m_btnUploadMicro.Size = new System.Drawing.Size(172, 41);
            this.m_btnUploadMicro.TabIndex = 66;
            this.m_btnUploadMicro.Text = "UPLOAD IMAGES...";
            this.m_btnUploadMicro.UseVisualStyleBackColor = true;
            this.m_btnUploadMicro.Click += new System.EventHandler(this.m_btnUploadMicro_Click);
            // 
            // m_pbRearMicro
            // 
            this.m_pbRearMicro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_pbRearMicro.Location = new System.Drawing.Point(348, 10);
            this.m_pbRearMicro.Name = "m_pbRearMicro";
            this.m_pbRearMicro.Size = new System.Drawing.Size(302, 101);
            this.m_pbRearMicro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.m_pbRearMicro.TabIndex = 65;
            this.m_pbRearMicro.TabStop = false;
            // 
            // m_pbFrontMicro
            // 
            this.m_pbFrontMicro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.m_pbFrontMicro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_pbFrontMicro.Location = new System.Drawing.Point(14, 10);
            this.m_pbFrontMicro.Name = "m_pbFrontMicro";
            this.m_pbFrontMicro.Size = new System.Drawing.Size(302, 101);
            this.m_pbFrontMicro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.m_pbFrontMicro.TabIndex = 64;
            this.m_pbFrontMicro.TabStop = false;
            // 
            // m_btnBrowseRearImageMicro
            // 
            this.m_btnBrowseRearImageMicro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_btnBrowseRearImageMicro.Location = new System.Drawing.Point(348, 117);
            this.m_btnBrowseRearImageMicro.Name = "m_btnBrowseRearImageMicro";
            this.m_btnBrowseRearImageMicro.Size = new System.Drawing.Size(162, 23);
            this.m_btnBrowseRearImageMicro.TabIndex = 63;
            this.m_btnBrowseRearImageMicro.Text = "Select Rear Image...";
            this.m_btnBrowseRearImageMicro.UseVisualStyleBackColor = true;
            this.m_btnBrowseRearImageMicro.Click += new System.EventHandler(this.m_btnBrowseRearImageMicro_Click);
            // 
            // m_btnBrowseFrontImageMicro
            // 
            this.m_btnBrowseFrontImageMicro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_btnBrowseFrontImageMicro.Location = new System.Drawing.Point(14, 117);
            this.m_btnBrowseFrontImageMicro.Name = "m_btnBrowseFrontImageMicro";
            this.m_btnBrowseFrontImageMicro.Size = new System.Drawing.Size(162, 23);
            this.m_btnBrowseFrontImageMicro.TabIndex = 62;
            this.m_btnBrowseFrontImageMicro.Text = "Select Front Image...";
            this.m_btnBrowseFrontImageMicro.UseVisualStyleBackColor = true;
            this.m_btnBrowseFrontImageMicro.Click += new System.EventHandler(this.m_btnBrowseFrontImageMicro_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.m_tabControl);
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(773, 551);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "Global Gateway Services";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(781, 580);
            this.tabControl1.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(512, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 17);
            this.label9.TabIndex = 35;
            this.label9.Text = "-       (dash)";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::ACHeck21_Web_Services_Demo.Properties.Resources.dash;
            this.pictureBox6.InitialImage = global::ACHeck21_Web_Services_Demo.Properties.Resources.transit;
            this.pictureBox6.Location = new System.Drawing.Point(458, 14);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(27, 29);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox6.TabIndex = 34;
            this.pictureBox6.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(284, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 17);
            this.label10.TabIndex = 36;
            this.label10.Text = "c       (on-us)";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::ACHeck21_Web_Services_Demo.Properties.Resources.onus;
            this.pictureBox7.InitialImage = global::ACHeck21_Web_Services_Demo.Properties.Resources.transit;
            this.pictureBox7.Location = new System.Drawing.Point(230, 16);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(27, 27);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox7.TabIndex = 33;
            this.pictureBox7.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(62, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 17);
            this.label11.TabIndex = 32;
            this.label11.Text = "d       (transit)";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::ACHeck21_Web_Services_Demo.Properties.Resources.transit;
            this.pictureBox8.InitialImage = global::ACHeck21_Web_Services_Demo.Properties.Resources.transit;
            this.pictureBox8.Location = new System.Drawing.Point(8, 16);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(27, 27);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 31;
            this.pictureBox8.TabStop = false;
            // 
            // m_openDlgBatch
            // 
            this.m_openDlgBatch.Filter = "CSV files|*.csv|XML files|*.xml";
            // 
            // m_tabReturns
            // 
            this.m_tabReturns.Controls.Add(this.panel4);
            this.m_tabReturns.Controls.Add(this.panel3);
            this.m_tabReturns.Location = new System.Drawing.Point(4, 22);
            this.m_tabReturns.Name = "m_tabReturns";
            this.m_tabReturns.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabReturns.Size = new System.Drawing.Size(759, 519);
            this.m_tabReturns.TabIndex = 1;
            this.m_tabReturns.Text = "Returns";
            this.m_tabReturns.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.m_btnGetReturns);
            this.panel3.Controls.Add(this.m_cbClientsReturns);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.m_lblReturnsTo);
            this.panel3.Controls.Add(this.m_lblReturnsFrom);
            this.panel3.Controls.Add(this.m_dtpReturnsTo);
            this.panel3.Controls.Add(this.m_dtpReturnsFrom);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(753, 77);
            this.panel3.TabIndex = 0;
            // 
            // m_dtpReturnsFrom
            // 
            this.m_dtpReturnsFrom.Location = new System.Drawing.Point(5, 16);
            this.m_dtpReturnsFrom.Name = "m_dtpReturnsFrom";
            this.m_dtpReturnsFrom.Size = new System.Drawing.Size(200, 20);
            this.m_dtpReturnsFrom.TabIndex = 5;
            // 
            // m_dtpReturnsTo
            // 
            this.m_dtpReturnsTo.Location = new System.Drawing.Point(228, 16);
            this.m_dtpReturnsTo.Name = "m_dtpReturnsTo";
            this.m_dtpReturnsTo.Size = new System.Drawing.Size(200, 20);
            this.m_dtpReturnsTo.TabIndex = 6;
            // 
            // m_lblReturnsFrom
            // 
            this.m_lblReturnsFrom.AutoSize = true;
            this.m_lblReturnsFrom.Location = new System.Drawing.Point(5, 0);
            this.m_lblReturnsFrom.Name = "m_lblReturnsFrom";
            this.m_lblReturnsFrom.Size = new System.Drawing.Size(33, 13);
            this.m_lblReturnsFrom.TabIndex = 7;
            this.m_lblReturnsFrom.Text = "From:";
            // 
            // m_lblReturnsTo
            // 
            this.m_lblReturnsTo.AutoSize = true;
            this.m_lblReturnsTo.Location = new System.Drawing.Point(225, 0);
            this.m_lblReturnsTo.Name = "m_lblReturnsTo";
            this.m_lblReturnsTo.Size = new System.Drawing.Size(23, 13);
            this.m_lblReturnsTo.TabIndex = 8;
            this.m_lblReturnsTo.Text = "To:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(212, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "-";
            // 
            // m_cbClientsReturns
            // 
            this.m_cbClientsReturns.FormattingEnabled = true;
            this.m_cbClientsReturns.Location = new System.Drawing.Point(5, 42);
            this.m_cbClientsReturns.Name = "m_cbClientsReturns";
            this.m_cbClientsReturns.Size = new System.Drawing.Size(217, 21);
            this.m_cbClientsReturns.TabIndex = 10;
            // 
            // m_btnGetReturns
            // 
            this.m_btnGetReturns.Location = new System.Drawing.Point(353, 42);
            this.m_btnGetReturns.Name = "m_btnGetReturns";
            this.m_btnGetReturns.Size = new System.Drawing.Size(75, 23);
            this.m_btnGetReturns.TabIndex = 11;
            this.m_btnGetReturns.Text = "Search";
            this.m_btnGetReturns.UseVisualStyleBackColor = true;
            this.m_btnGetReturns.Click += new System.EventHandler(this.m_btnGetReturns_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.m_dgvReturns);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 80);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(753, 436);
            this.panel4.TabIndex = 1;
            // 
            // m_dgvReturns
            // 
            this.m_dgvReturns.AllowUserToAddRows = false;
            this.m_dgvReturns.AllowUserToDeleteRows = false;
            this.m_dgvReturns.AllowUserToOrderColumns = true;
            this.m_dgvReturns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvReturns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_dgvReturns.Location = new System.Drawing.Point(0, 0);
            this.m_dgvReturns.Name = "m_dgvReturns";
            this.m_dgvReturns.ReadOnly = true;
            this.m_dgvReturns.RowHeadersVisible = false;
            this.m_dgvReturns.Size = new System.Drawing.Size(753, 436);
            this.m_dgvReturns.TabIndex = 0;
            // 
            // m_tabTransactions
            // 
            this.m_tabTransactions.Controls.Add(this.panel2);
            this.m_tabTransactions.Controls.Add(this.panel1);
            this.m_tabTransactions.Location = new System.Drawing.Point(4, 22);
            this.m_tabTransactions.Name = "m_tabTransactions";
            this.m_tabTransactions.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabTransactions.Size = new System.Drawing.Size(759, 519);
            this.m_tabTransactions.TabIndex = 0;
            this.m_tabTransactions.Text = "Transactions";
            this.m_tabTransactions.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.m_btnGetTrans);
            this.panel1.Controls.Add(this.m_cbClientsTrans);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.m_lblTransTo);
            this.panel1.Controls.Add(this.m_lblTransFrom);
            this.panel1.Controls.Add(this.m_dtpTransTo);
            this.panel1.Controls.Add(this.m_dtpTransFrom);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(753, 77);
            this.panel1.TabIndex = 0;
            // 
            // m_dtpTransFrom
            // 
            this.m_dtpTransFrom.Location = new System.Drawing.Point(5, 16);
            this.m_dtpTransFrom.Name = "m_dtpTransFrom";
            this.m_dtpTransFrom.Size = new System.Drawing.Size(200, 20);
            this.m_dtpTransFrom.TabIndex = 0;
            // 
            // m_dtpTransTo
            // 
            this.m_dtpTransTo.Location = new System.Drawing.Point(228, 16);
            this.m_dtpTransTo.Name = "m_dtpTransTo";
            this.m_dtpTransTo.Size = new System.Drawing.Size(200, 20);
            this.m_dtpTransTo.TabIndex = 1;
            // 
            // m_lblTransFrom
            // 
            this.m_lblTransFrom.AutoSize = true;
            this.m_lblTransFrom.Location = new System.Drawing.Point(5, 0);
            this.m_lblTransFrom.Name = "m_lblTransFrom";
            this.m_lblTransFrom.Size = new System.Drawing.Size(33, 13);
            this.m_lblTransFrom.TabIndex = 2;
            this.m_lblTransFrom.Text = "From:";
            // 
            // m_lblTransTo
            // 
            this.m_lblTransTo.AutoSize = true;
            this.m_lblTransTo.Location = new System.Drawing.Point(225, 0);
            this.m_lblTransTo.Name = "m_lblTransTo";
            this.m_lblTransTo.Size = new System.Drawing.Size(23, 13);
            this.m_lblTransTo.TabIndex = 3;
            this.m_lblTransTo.Text = "To:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(212, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "-";
            // 
            // m_cbClientsTrans
            // 
            this.m_cbClientsTrans.FormattingEnabled = true;
            this.m_cbClientsTrans.Location = new System.Drawing.Point(5, 42);
            this.m_cbClientsTrans.Name = "m_cbClientsTrans";
            this.m_cbClientsTrans.Size = new System.Drawing.Size(217, 21);
            this.m_cbClientsTrans.TabIndex = 5;
            // 
            // m_btnGetTrans
            // 
            this.m_btnGetTrans.Location = new System.Drawing.Point(353, 42);
            this.m_btnGetTrans.Name = "m_btnGetTrans";
            this.m_btnGetTrans.Size = new System.Drawing.Size(75, 23);
            this.m_btnGetTrans.TabIndex = 12;
            this.m_btnGetTrans.Text = "Search";
            this.m_btnGetTrans.UseVisualStyleBackColor = true;
            this.m_btnGetTrans.Click += new System.EventHandler(this.m_btnGetTrans_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.m_dgvTrans);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 80);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(753, 436);
            this.panel2.TabIndex = 1;
            // 
            // m_dgvTrans
            // 
            this.m_dgvTrans.AllowUserToAddRows = false;
            this.m_dgvTrans.AllowUserToDeleteRows = false;
            this.m_dgvTrans.AllowUserToOrderColumns = true;
            this.m_dgvTrans.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.m_dgvTrans.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_dgvTrans.Location = new System.Drawing.Point(0, 0);
            this.m_dgvTrans.Name = "m_dgvTrans";
            this.m_dgvTrans.ReadOnly = true;
            this.m_dgvTrans.Size = new System.Drawing.Size(753, 436);
            this.m_dgvTrans.TabIndex = 0;
            // 
            // m_tabSendBatch
            // 
            this.m_tabSendBatch.Controls.Add(this.m_btnSendBatch);
            this.m_tabSendBatch.Controls.Add(this.m_tbBatchContents);
            this.m_tabSendBatch.Controls.Add(this.m_tbBatchFile);
            this.m_tabSendBatch.Controls.Add(this.label34);
            this.m_tabSendBatch.Controls.Add(this.m_btnChooseBatch);
            this.m_tabSendBatch.Location = new System.Drawing.Point(4, 22);
            this.m_tabSendBatch.Name = "m_tabSendBatch";
            this.m_tabSendBatch.Size = new System.Drawing.Size(759, 519);
            this.m_tabSendBatch.TabIndex = 4;
            this.m_tabSendBatch.Text = "Batch File";
            this.m_tabSendBatch.UseVisualStyleBackColor = true;
            // 
            // m_tbBatchFile
            // 
            this.m_tbBatchFile.Location = new System.Drawing.Point(15, 55);
            this.m_tbBatchFile.Name = "m_tbBatchFile";
            this.m_tbBatchFile.ReadOnly = true;
            this.m_tbBatchFile.Size = new System.Drawing.Size(407, 20);
            this.m_tbBatchFile.TabIndex = 0;
            // 
            // m_btnChooseBatch
            // 
            this.m_btnChooseBatch.Location = new System.Drawing.Point(428, 53);
            this.m_btnChooseBatch.Name = "m_btnChooseBatch";
            this.m_btnChooseBatch.Size = new System.Drawing.Size(29, 23);
            this.m_btnChooseBatch.TabIndex = 1;
            this.m_btnChooseBatch.Text = "...";
            this.m_btnChooseBatch.UseVisualStyleBackColor = true;
            this.m_btnChooseBatch.Click += new System.EventHandler(this.m_btnChooseBatch_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(12, 39);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(52, 13);
            this.label34.TabIndex = 2;
            this.label34.Text = "Filename:";
            // 
            // m_tbBatchContents
            // 
            this.m_tbBatchContents.Location = new System.Drawing.Point(15, 81);
            this.m_tbBatchContents.Multiline = true;
            this.m_tbBatchContents.Name = "m_tbBatchContents";
            this.m_tbBatchContents.ReadOnly = true;
            this.m_tbBatchContents.Size = new System.Drawing.Size(727, 366);
            this.m_tbBatchContents.TabIndex = 3;
            // 
            // m_btnSendBatch
            // 
            this.m_btnSendBatch.Location = new System.Drawing.Point(591, 453);
            this.m_btnSendBatch.Name = "m_btnSendBatch";
            this.m_btnSendBatch.Size = new System.Drawing.Size(151, 37);
            this.m_btnSendBatch.TabIndex = 4;
            this.m_btnSendBatch.Text = "SEND BATCH";
            this.m_btnSendBatch.UseVisualStyleBackColor = true;
            this.m_btnSendBatch.Click += new System.EventHandler(this.m_btnSendBatch_Click);
            // 
            // m_tabCreateCheck
            // 
            this.m_tabCreateCheck.Controls.Add(this.label29);
            this.m_tabCreateCheck.Controls.Add(this.m_cmbxProtocol);
            this.m_tabCreateCheck.Controls.Add(this.m_gbLegend);
            this.m_tabCreateCheck.Controls.Add(this.label25);
            this.m_tabCreateCheck.Controls.Add(this.m_tbMicrGG);
            this.m_tabCreateCheck.Controls.Add(this.m_tbPayTo);
            this.m_tabCreateCheck.Controls.Add(this.m_tbAmountGG);
            this.m_tabCreateCheck.Controls.Add(this.m_tbCheckNrGG);
            this.m_tabCreateCheck.Controls.Add(this.m_tbAccountNrGG);
            this.m_tabCreateCheck.Controls.Add(this.m_tbRoutingNrGG);
            this.m_tabCreateCheck.Controls.Add(this.label24);
            this.m_tabCreateCheck.Controls.Add(this.m_cmbxMethodTarget);
            this.m_tabCreateCheck.Controls.Add(this.label23);
            this.m_tabCreateCheck.Controls.Add(this.m_cmbxEntryClass);
            this.m_tabCreateCheck.Controls.Add(this.m_pbRearGG);
            this.m_tabCreateCheck.Controls.Add(this.m_pbFrontGG);
            this.m_tabCreateCheck.Controls.Add(this.m_btnBrowseRearImageGG);
            this.m_tabCreateCheck.Controls.Add(this.m_btnBrowseFrontImageGG);
            this.m_tabCreateCheck.Controls.Add(this.label8);
            this.m_tabCreateCheck.Controls.Add(this.label7);
            this.m_tabCreateCheck.Controls.Add(this.m_cbCheckFrom);
            this.m_tabCreateCheck.Controls.Add(this.m_btnCreateCheck);
            this.m_tabCreateCheck.Controls.Add(this.label6);
            this.m_tabCreateCheck.Controls.Add(this.label5);
            this.m_tabCreateCheck.Controls.Add(this.label4);
            this.m_tabCreateCheck.Controls.Add(this.label3);
            this.m_tabCreateCheck.Location = new System.Drawing.Point(4, 22);
            this.m_tabCreateCheck.Name = "m_tabCreateCheck";
            this.m_tabCreateCheck.Padding = new System.Windows.Forms.Padding(3);
            this.m_tabCreateCheck.Size = new System.Drawing.Size(759, 519);
            this.m_tabCreateCheck.TabIndex = 3;
            this.m_tabCreateCheck.Text = "Create Check";
            this.m_tabCreateCheck.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Routing Number";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(251, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Account Number";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(470, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Check Number";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(541, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Amount";
            // 
            // m_btnCreateCheck
            // 
            this.m_btnCreateCheck.Location = new System.Drawing.Point(551, 464);
            this.m_btnCreateCheck.Name = "m_btnCreateCheck";
            this.m_btnCreateCheck.Size = new System.Drawing.Size(192, 37);
            this.m_btnCreateCheck.TabIndex = 8;
            this.m_btnCreateCheck.Text = "CREATE";
            this.m_btnCreateCheck.UseVisualStyleBackColor = true;
            this.m_btnCreateCheck.Click += new System.EventHandler(this.m_btnCreateCheck_Click);
            // 
            // m_cbCheckFrom
            // 
            this.m_cbCheckFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_cbCheckFrom.FormattingEnabled = true;
            this.m_cbCheckFrom.Location = new System.Drawing.Point(36, 38);
            this.m_cbCheckFrom.Name = "m_cbCheckFrom";
            this.m_cbCheckFrom.Size = new System.Drawing.Size(217, 21);
            this.m_cbCheckFrom.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(33, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "From";
            // 
            // m_tbRoutingNrGG
            // 
            this.m_tbRoutingNrGG.Location = new System.Drawing.Point(36, 159);
            this.m_tbRoutingNrGG.Name = "m_tbRoutingNrGG";
            this.m_tbRoutingNrGG.Size = new System.Drawing.Size(189, 20);
            this.m_tbRoutingNrGG.TabIndex = 0;
            // 
            // m_tbAccountNrGG
            // 
            this.m_tbAccountNrGG.Location = new System.Drawing.Point(254, 159);
            this.m_tbAccountNrGG.Name = "m_tbAccountNrGG";
            this.m_tbAccountNrGG.Size = new System.Drawing.Size(195, 20);
            this.m_tbAccountNrGG.TabIndex = 1;
            // 
            // m_tbCheckNrGG
            // 
            this.m_tbCheckNrGG.Location = new System.Drawing.Point(473, 159);
            this.m_tbCheckNrGG.Name = "m_tbCheckNrGG";
            this.m_tbCheckNrGG.Size = new System.Drawing.Size(147, 20);
            this.m_tbCheckNrGG.TabIndex = 2;
            // 
            // m_tbAmountGG
            // 
            this.m_tbAmountGG.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_tbAmountGG.Location = new System.Drawing.Point(544, 96);
            this.m_tbAmountGG.Name = "m_tbAmountGG";
            this.m_tbAmountGG.Size = new System.Drawing.Size(165, 29);
            this.m_tbAmountGG.TabIndex = 6;
            // 
            // m_tbPayTo
            // 
            this.m_tbPayTo.Location = new System.Drawing.Point(36, 105);
            this.m_tbPayTo.Name = "m_tbPayTo";
            this.m_tbPayTo.Size = new System.Drawing.Size(283, 20);
            this.m_tbPayTo.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(33, 86);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Pay To The Order Of:";
            // 
            // m_btnBrowseFrontImageGG
            // 
            this.m_btnBrowseFrontImageGG.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_btnBrowseFrontImageGG.Location = new System.Drawing.Point(36, 427);
            this.m_btnBrowseFrontImageGG.Name = "m_btnBrowseFrontImageGG";
            this.m_btnBrowseFrontImageGG.Size = new System.Drawing.Size(162, 23);
            this.m_btnBrowseFrontImageGG.TabIndex = 14;
            this.m_btnBrowseFrontImageGG.Text = "Select Front Image...";
            this.m_btnBrowseFrontImageGG.UseVisualStyleBackColor = true;
            this.m_btnBrowseFrontImageGG.Click += new System.EventHandler(this.m_btnBrowseFrontImageGG_Click);
            // 
            // m_btnBrowseRearImageGG
            // 
            this.m_btnBrowseRearImageGG.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.m_btnBrowseRearImageGG.Location = new System.Drawing.Point(370, 427);
            this.m_btnBrowseRearImageGG.Name = "m_btnBrowseRearImageGG";
            this.m_btnBrowseRearImageGG.Size = new System.Drawing.Size(162, 23);
            this.m_btnBrowseRearImageGG.TabIndex = 16;
            this.m_btnBrowseRearImageGG.Text = "Select Rear Image...";
            this.m_btnBrowseRearImageGG.UseVisualStyleBackColor = true;
            this.m_btnBrowseRearImageGG.Click += new System.EventHandler(this.m_btnBrowseRearImageGG_Click);
            // 
            // m_pbFrontGG
            // 
            this.m_pbFrontGG.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.m_pbFrontGG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_pbFrontGG.Location = new System.Drawing.Point(36, 320);
            this.m_pbFrontGG.Name = "m_pbFrontGG";
            this.m_pbFrontGG.Size = new System.Drawing.Size(302, 101);
            this.m_pbFrontGG.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.m_pbFrontGG.TabIndex = 17;
            this.m_pbFrontGG.TabStop = false;
            // 
            // m_pbRearGG
            // 
            this.m_pbRearGG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_pbRearGG.Location = new System.Drawing.Point(370, 320);
            this.m_pbRearGG.Name = "m_pbRearGG";
            this.m_pbRearGG.Size = new System.Drawing.Size(302, 101);
            this.m_pbRearGG.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.m_pbRearGG.TabIndex = 18;
            this.m_pbRearGG.TabStop = false;
            // 
            // m_cmbxEntryClass
            // 
            this.m_cmbxEntryClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_cmbxEntryClass.FormattingEnabled = true;
            this.m_cmbxEntryClass.Location = new System.Drawing.Point(497, 38);
            this.m_cmbxEntryClass.Name = "m_cmbxEntryClass";
            this.m_cmbxEntryClass.Size = new System.Drawing.Size(212, 21);
            this.m_cmbxEntryClass.TabIndex = 21;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(494, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(87, 13);
            this.label23.TabIndex = 22;
            this.label23.Text = "Entry Class Code";
            // 
            // m_cmbxMethodTarget
            // 
            this.m_cmbxMethodTarget.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_cmbxMethodTarget.FormattingEnabled = true;
            this.m_cmbxMethodTarget.Location = new System.Drawing.Point(230, 480);
            this.m_cmbxMethodTarget.Name = "m_cmbxMethodTarget";
            this.m_cmbxMethodTarget.Size = new System.Drawing.Size(302, 21);
            this.m_cmbxMethodTarget.TabIndex = 23;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(227, 464);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(156, 13);
            this.label24.TabIndex = 24;
            this.label24.Text = "Web Method Invocation Target";
            // 
            // m_tbMicrGG
            // 
            this.m_tbMicrGG.Location = new System.Drawing.Point(36, 212);
            this.m_tbMicrGG.Name = "m_tbMicrGG";
            this.m_tbMicrGG.Size = new System.Drawing.Size(413, 20);
            this.m_tbMicrGG.TabIndex = 25;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(33, 196);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(57, 13);
            this.label25.TabIndex = 26;
            this.label25.Text = "MICR Line";
            // 
            // m_gbLegend
            // 
            this.m_gbLegend.Controls.Add(this.label28);
            this.m_gbLegend.Controls.Add(this.pictureBox3);
            this.m_gbLegend.Controls.Add(this.label27);
            this.m_gbLegend.Controls.Add(this.pictureBox2);
            this.m_gbLegend.Controls.Add(this.label26);
            this.m_gbLegend.Controls.Add(this.pictureBox1);
            this.m_gbLegend.Location = new System.Drawing.Point(36, 248);
            this.m_gbLegend.Name = "m_gbLegend";
            this.m_gbLegend.Size = new System.Drawing.Size(602, 51);
            this.m_gbLegend.TabIndex = 27;
            this.m_gbLegend.TabStop = false;
            this.m_gbLegend.Text = "MICR Symbol Conversion Legend";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ACHeck21_Web_Services_Demo.Properties.Resources.transit;
            this.pictureBox1.InitialImage = global::ACHeck21_Web_Services_Demo.Properties.Resources.transit;
            this.pictureBox1.Location = new System.Drawing.Point(8, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(27, 27);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 31;
            this.pictureBox1.TabStop = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(62, 22);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(93, 17);
            this.label26.TabIndex = 32;
            this.label26.Text = "d       (transit)";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::ACHeck21_Web_Services_Demo.Properties.Resources.onus;
            this.pictureBox2.InitialImage = global::ACHeck21_Web_Services_Demo.Properties.Resources.transit;
            this.pictureBox2.Location = new System.Drawing.Point(230, 16);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(27, 27);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 33;
            this.pictureBox2.TabStop = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(284, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(89, 17);
            this.label27.TabIndex = 36;
            this.label27.Text = "c       (on-us)";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::ACHeck21_Web_Services_Demo.Properties.Resources.dash;
            this.pictureBox3.InitialImage = global::ACHeck21_Web_Services_Demo.Properties.Resources.transit;
            this.pictureBox3.Location = new System.Drawing.Point(458, 14);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(27, 29);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 34;
            this.pictureBox3.TabStop = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(512, 20);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(82, 17);
            this.label28.TabIndex = 35;
            this.label28.Text = "-       (dash)";
            // 
            // m_cmbxProtocol
            // 
            this.m_cmbxProtocol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.m_cmbxProtocol.FormattingEnabled = true;
            this.m_cmbxProtocol.Items.AddRange(new object[] {
            "SOAP",
            "REST"});
            this.m_cmbxProtocol.Location = new System.Drawing.Point(36, 480);
            this.m_cmbxProtocol.Name = "m_cmbxProtocol";
            this.m_cmbxProtocol.Size = new System.Drawing.Size(162, 21);
            this.m_cmbxProtocol.TabIndex = 28;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(33, 464);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(111, 13);
            this.label29.TabIndex = 29;
            this.label29.Text = "Web Service Protocol";
            // 
            // m_tabControl
            // 
            this.m_tabControl.Controls.Add(this.m_tabCreateCheck);
            this.m_tabControl.Controls.Add(this.m_tabSendBatch);
            this.m_tabControl.Controls.Add(this.m_tabTransactions);
            this.m_tabControl.Controls.Add(this.m_tabReturns);
            this.m_tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_tabControl.Location = new System.Drawing.Point(3, 3);
            this.m_tabControl.Name = "m_tabControl";
            this.m_tabControl.SelectedIndex = 0;
            this.m_tabControl.Size = new System.Drawing.Size(767, 545);
            this.m_tabControl.TabIndex = 1;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(781, 580);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.m_dsReturns)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.m_gbMicroCheck.ResumeLayout(false);
            this.m_gbMicroCheck.PerformLayout();
            this.m_pnlMicroCheck.ResumeLayout(false);
            this.m_pnlMicroCheck.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pbRearMicro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pbFrontMicro)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.m_tabReturns.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvReturns)).EndInit();
            this.m_tabTransactions.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_dgvTrans)).EndInit();
            this.m_tabSendBatch.ResumeLayout(false);
            this.m_tabSendBatch.PerformLayout();
            this.m_tabCreateCheck.ResumeLayout(false);
            this.m_tabCreateCheck.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_pbFrontGG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_pbRearGG)).EndInit();
            this.m_gbLegend.ResumeLayout(false);
            this.m_gbLegend.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.m_tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Data.DataSet m_dsReturns;
        private System.Windows.Forms.OpenFileDialog m_openDlgImage;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TextBox m_tbQueryLog;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel m_pnlMicroCheck;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox m_cbMicroAccount;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox m_tbHwMICR;
        private System.Windows.Forms.Button m_btnUploadMicro;
        private System.Windows.Forms.PictureBox m_pbRearMicro;
        private System.Windows.Forms.PictureBox m_pbFrontMicro;
        private System.Windows.Forms.Button m_btnBrowseRearImageMicro;
        private System.Windows.Forms.Button m_btnBrowseFrontImageMicro;
        private System.Windows.Forms.GroupBox m_gbMicroCheck;
        private System.Windows.Forms.Button m_btnMicroDelete;
        private System.Windows.Forms.Label m_lblRecordNumber;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button m_btnCorrect;
        private System.Windows.Forms.CheckBox m_cbMICR_OK;
        private System.Windows.Forms.CheckBox m_cbIQA;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox m_tbMergedMICR;
        private System.Windows.Forms.TextBox m_tbCarLarAmount;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox m_tbCheckNrMicro;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox m_tbAccountNrMicro;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox m_tbRoutingNrMicro;
        private System.Windows.Forms.Label label17;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.OvalShape m_osStatus;
        private System.Windows.Forms.CheckBox m_cbDuplicate;
        private System.Windows.Forms.LinkLabel m_llMicroGateway;
        private System.Windows.Forms.OpenFileDialog m_openDlgBatch;
        private System.Windows.Forms.TabControl m_tabControl;
        private System.Windows.Forms.TabPage m_tabCreateCheck;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox m_cmbxProtocol;
        private System.Windows.Forms.GroupBox m_gbLegend;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox m_tbMicrGG;
        private System.Windows.Forms.TextBox m_tbPayTo;
        private System.Windows.Forms.TextBox m_tbAmountGG;
        private System.Windows.Forms.TextBox m_tbCheckNrGG;
        private System.Windows.Forms.TextBox m_tbAccountNrGG;
        private System.Windows.Forms.TextBox m_tbRoutingNrGG;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox m_cmbxMethodTarget;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox m_cmbxEntryClass;
        private System.Windows.Forms.PictureBox m_pbRearGG;
        private System.Windows.Forms.PictureBox m_pbFrontGG;
        private System.Windows.Forms.Button m_btnBrowseRearImageGG;
        private System.Windows.Forms.Button m_btnBrowseFrontImageGG;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox m_cbCheckFrom;
        private System.Windows.Forms.Button m_btnCreateCheck;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage m_tabSendBatch;
        private System.Windows.Forms.Button m_btnSendBatch;
        private System.Windows.Forms.TextBox m_tbBatchContents;
        private System.Windows.Forms.TextBox m_tbBatchFile;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button m_btnChooseBatch;
        private System.Windows.Forms.TabPage m_tabTransactions;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView m_dgvTrans;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button m_btnGetTrans;
        private System.Windows.Forms.ComboBox m_cbClientsTrans;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label m_lblTransTo;
        private System.Windows.Forms.Label m_lblTransFrom;
        private System.Windows.Forms.DateTimePicker m_dtpTransTo;
        private System.Windows.Forms.DateTimePicker m_dtpTransFrom;
        private System.Windows.Forms.TabPage m_tabReturns;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView m_dgvReturns;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button m_btnGetReturns;
        private System.Windows.Forms.ComboBox m_cbClientsReturns;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label m_lblReturnsTo;
        private System.Windows.Forms.Label m_lblReturnsFrom;
        private System.Windows.Forms.DateTimePicker m_dtpReturnsTo;
        private System.Windows.Forms.DateTimePicker m_dtpReturnsFrom;


    }
}

