﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACHeck21_Web_Services_Demo {
    public class QueryBetween : IQueryOperation {
        private string m_field;
        private string m_lValue;
        private string m_hValue;

        public QueryBetween(string field, string lValue, string hValue) {
            m_field = field;
            m_lValue = lValue;
            m_hValue = hValue;
        }

        #region IQueryOperation Members

        public string GenerateXML() {
            StringBuilder sb = new StringBuilder();
            sb.Append("<").Append(m_field).Append(">\n");
            sb.Append("\t<Operation>Between</Operation>\n");
            sb.Append("\t<Value>").Append(m_lValue).Append("</Value>\n");
            sb.Append("\t<Value>").Append(m_hValue).Append("</Value>\n");
            sb.Append("</").Append(m_field).Append(">\n");
            return sb.ToString();
        }

        string IQueryOperation.GenerateXML() {
            return GenerateXML();
        }

        public string GenerateGET() {
            StringBuilder sb = new StringBuilder();
            sb.Append(m_field).Append("=between,").Append(m_lValue).Append(",").Append(m_hValue);
            return sb.ToString();
        }

        string IQueryOperation.GenerateGET() {
            return GenerateGET();
        }

        #endregion

    }
}
