﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ACHeck21_Web_Services_Demo {
    public class GlobalGatewaySOAP : IGlobalGateway {
        private static com.acheck21.gateway.ACHeck21GatewayVersion24 _gateway = new ACHeck21_Web_Services_Demo.com.acheck21.gateway.ACHeck21GatewayVersion24();
        private string m_username = "", m_password = "";

        private string Encode(string fn) {
            byte[] data;
            if (File.Exists(fn)) {
                data = File.ReadAllBytes(fn);
                return Convert.ToBase64String(data);
            } else
                throw new FileNotFoundException(fn);
        }

        public GlobalGatewaySOAP(string username, string password) {
            m_username = username;
            m_password = password;
        }

        #region IGlobalGateway Members

        public com.acheck21.gateway.AuthenticateResult Authenticate() {
            return _gateway.Authenticate(m_username, m_password);
        }

        public com.acheck21.gateway.FindUserClientsResult FindUserClients(string username) {
            return _gateway.FindUserClients(m_username, m_password, username);
        }

        public com.acheck21.gateway.FindChecksDetailsResult FindChecksDetails(string clientId, string query) {
            return _gateway.FindChecksDetails(m_username, m_password, clientId, query);
        }

        public com.acheck21.gateway.FindReturnsDetailsResult FindReturnsDetails(string clientId, string query) {
            return  _gateway.FindReturnsDetails(m_username, m_password, clientId, query);
        }

        public com.acheck21.gateway.AuthorizeCheckResult AuthorizeCheck(string clientId, string micr, string routingNr, string dda,
            string checkNr, decimal amount, string dlNumber, string phone) {

            return _gateway.AuthorizeCheck(m_username, m_password, clientId, micr, routingNr, dda, checkNr, amount, dlNumber, phone);
        }

        public com.acheck21.gateway.CreateCheckResult CreateCheck(string clientId, string clientTag, string payee, string checkNr, string routingNr, string dda,
            decimal amount, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass ecc, string frontFn, string rearFn,
            string micr, DateTime postDate, string[] addenda) {

            string front = "", rear = "";
            // Get Base64 encoded string of image data
            if (File.Exists(frontFn)) {
                front = Encode(frontFn);
            }
            if (File.Exists(rearFn)) {
                rear = Encode(rearFn);
            }

            return _gateway.CreateCheck(m_username, m_password, clientId.Trim(), clientTag, payee.Trim(),
                    checkNr.Trim(), routingNr.Trim(), dda.Trim(), ACHeck21_Web_Services_Demo.com.acheck21.gateway.AccountType.Checking,
                    Convert.ToString(amount), (ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass)ecc, front, rear,
                    micr.Trim(), postDate.ToShortDateString(), addenda);
        }

        public com.acheck21.gateway.CreateValidatedCheckResult CreateValidatedCheck(string clientId, string clientTag, string payee, string checkNr, string routingNr, string dda,
            decimal amount, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass ecc, string frontFn, string rearFn, string micr, string dlNumber,
            string phone, DateTime postDate, string[] addenda) {

            string front = "", rear = "";
            // Get Base64 encoded string of image data
            if (File.Exists(frontFn)) {
                front = Encode(frontFn);
            }
            if (File.Exists(rearFn)) {
                rear = Encode(rearFn);
            }

            return _gateway.CreateValidatedCheck(m_username, m_password, clientId.Trim(), clientTag, payee.Trim(),
                    checkNr.Trim(), routingNr.Trim(), dda.Trim(), ACHeck21_Web_Services_Demo.com.acheck21.gateway.AccountType.Checking,
                    Convert.ToString(amount), (ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass)ecc, front, rear,
                    micr.Trim(), dlNumber.Trim(), phone.Trim(), postDate.ToShortDateString(), addenda);
        }

        public com.acheck21.gateway.CreateValidatedCheckWithIRResult CreateValidatedCheckWithIR(string clientId, string payee, string checkNr, string routingNr, string dda,
             decimal amount, string frontFn, string rearFn, string micr, string dlNumber,
             string phone, DateTime postDate) {

            string front = "", rear = "";
            // Get Base64 encoded string of image data
            if (File.Exists(frontFn)) {
                front = Encode(frontFn);
            }
            if (File.Exists(rearFn)) {
                rear = Encode(rearFn);
            }

            return _gateway.CreateValidatedCheckWithIR(m_username, m_password, clientId.Trim(), payee.Trim(),
                    checkNr.Trim(), routingNr.Trim(), dda.Trim(), ACHeck21_Web_Services_Demo.com.acheck21.gateway.AccountType.Checking,
                    amount.ToString(), ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass.BOC, front, rear, micr.Trim(), dlNumber.Trim(), phone.Trim(), postDate.ToShortDateString());
        }

        public com.acheck21.gateway.CreateCheckWithIRResult CreateCheckWithIR(string clientId, string payee, string checkNr, string routingNr, string dda,
            decimal amount, string frontFn, string rearFn, string micr, DateTime postDate) {

            string front = "", rear = "";
            // Get Base64 encoded string of image data
            if (File.Exists(frontFn)) {
                front = Encode(frontFn);
            }
            if (File.Exists(rearFn)) {
                rear = Encode(rearFn);
            }

            return _gateway.CreateCheckWithIR(m_username, m_password, clientId.Trim(), payee.Trim(),
                    checkNr.Trim(), routingNr.Trim(), dda.Trim(), ACHeck21_Web_Services_Demo.com.acheck21.gateway.AccountType.Checking,
                    Convert.ToString(amount), ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass.PPD, front, rear, micr.Trim(), postDate.ToShortDateString());
        }

        public com.acheck21.gateway.CreateRCCResult CreateRCC(string clientId, string payee, string checkNr, string routingNr, string dda,
            decimal amount, DateTime checkDate, string payer, string addr1, string addr2, string addr3, string endorse1, string endorse2,
            string endorse3, string endorse4, string sigText) {
            return _gateway.CreateRCC(m_username, m_password, clientId.Trim(), payer, addr1, addr2, addr3, checkDate.ToShortDateString(), payee, Convert.ToString(amount), 
                routingNr, dda, checkNr, sigText, endorse1, endorse2, endorse3, endorse4);
        }

        public com.acheck21.gateway.SendBatchResults SendBatch(string clientId, string filename) {
            string b64data = "";
            if (File.Exists(filename)) {
                b64data = Encode(filename);
            } else
                throw new FileNotFoundException(filename);

            return _gateway.SendBatch(m_username, m_password, clientId.Trim(), b64data, Path.GetFileName(filename));
        }

        #endregion
    }

}
