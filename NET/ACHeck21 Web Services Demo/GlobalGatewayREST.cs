﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using System.Net;
using System.Xml;

namespace ACHeck21_Web_Services_Demo {
    public class GlobalGatewayREST : IGlobalGateway {
        private string m_username = "", m_password = "";

        private string EncodeImages(string imageFn) {
            byte[] imgData;
            if (File.Exists(imageFn)) {
                imgData = File.ReadAllBytes(imageFn);
                return Convert.ToBase64String(imgData);
            } else
                throw new FileNotFoundException("Unable to locate image file", imageFn);
        }

        private com.acheck21.gateway.GatewayServiceResult GetResponse(string url, string method, string queryString, out string response) {
            byte[] buf = Encoding.UTF8.GetBytes(queryString);

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = method;
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = buf.Length;

            if (req.Method == WebRequestMethods.Http.Post) {
                byte[] credentialBuffer = new UTF8Encoding().GetBytes(m_username + ":" + m_password);
                req.Headers["Authorization"] = "Basic " + Convert.ToBase64String(credentialBuffer);
            } else {
                NetworkCredential credentials = new NetworkCredential(m_username, m_password);
                CredentialCache cache = new CredentialCache();
                cache.Add(new Uri("https://gateway.acheck21.com"), "Basic", credentials);
                //cache.Add(new Uri("https://gateway.acheck21.com"), "Negotiate", credentials);
                req.Credentials = cache;
            }

            Stream reqst = req.GetRequestStream();
            reqst.Write(buf, 0, buf.Length);
            reqst.Flush();
            reqst.Close();

            com.acheck21.gateway.GatewayServiceResult result = new ACHeck21_Web_Services_Demo.com.acheck21.gateway.GatewayServiceResult();
            try {
                using (HttpWebResponse res = (HttpWebResponse)req.GetResponse()) {
                    Stream resst = res.GetResponseStream();
                    StreamReader sr = new StreamReader(resst);
                    response = sr.ReadToEnd();

                    result.Code = 0;
                    result.Message = "";
                }
            } catch (WebException ex) {
                using (HttpWebResponse res = (HttpWebResponse)ex.Response) {
                    response = "";
                    result.Code = Convert.ToInt32(res.Headers["Code"]);
                    result.Message = res.Headers["Message"];
                }
            }

            return result;
        }

        private string GetVariableString(List<KeyValuePair<string, string>> headerVars) {
            StringBuilder str = new StringBuilder();
            foreach (KeyValuePair<string, string> pair in headerVars) {
                if (pair.Key.Trim().Length > 0 && pair.Value.Length > 0)
                    str.Append(pair.Key.Trim()).Append("=").Append(pair.Value.Trim()).Append("&");
            }
            return str.ToString();
        }

        public GlobalGatewayREST(string username, string password) {
            m_username = username;
            m_password = password;
        }

        #region IGlobalGateway Members

        public ACHeck21_Web_Services_Demo.com.acheck21.gateway.AuthenticateResult Authenticate() {
            string response = "";
            com.acheck21.gateway.GatewayServiceResult gsr = GetResponse("https://gateway.acheck21.com/GlobalGateway/REST/check", WebRequestMethods.Http.Post, "", out response);

            com.acheck21.gateway.AuthenticateResult result = new ACHeck21_Web_Services_Demo.com.acheck21.gateway.AuthenticateResult();
            result.Code = gsr.Code;
            result.Message = gsr.Message;

            return result;
        }

        public ACHeck21_Web_Services_Demo.com.acheck21.gateway.AuthorizeCheckResult AuthorizeCheck(string clientId, string micr, string routingNr, string dda, string checkNr, decimal amount, string dlNumber, string phone) {
            List<KeyValuePair<string, string>> headerVars = new List<KeyValuePair<string, string>>() { 
                new KeyValuePair<string, string>("ClientID", clientId),
                new KeyValuePair<string, string>("MICR", micr),
                new KeyValuePair<string, string>("TransitNumber", routingNr),
                new KeyValuePair<string, string>("DDANumber", dda), 
                new KeyValuePair<string, string>("CheckNumber", checkNr),
                new KeyValuePair<string, string>("CheckAmount", amount.ToString("0.00")),
                new KeyValuePair<string, string>("DLNumber", dlNumber), 
                new KeyValuePair<string, string>("PhoneNumber", phone) };
            string query = GetVariableString(headerVars);

            string response = "";
            com.acheck21.gateway.GatewayServiceResult gsr = GetResponse("https://gateway.acheck21.com/GlobalGateway/REST/check/authorize", WebRequestMethods.Http.Post, query, out response);

            com.acheck21.gateway.AuthorizeCheckResult result = new ACHeck21_Web_Services_Demo.com.acheck21.gateway.AuthorizeCheckResult();
            result.Code = gsr.Code;
            result.Message = gsr.Message;
            result.Authorization = new ACHeck21_Web_Services_Demo.com.acheck21.gateway.AuthorizationResponse();

            if (result.Code == (int)Enums.ResponseCode.NO_ERROR) {
                XmlTextReader reader = new XmlTextReader(new StringReader(response));
                bool xmlResponse = false, xmlDetailLines = false, xmlAuthorizationCode = false;
                List<string> detailLines = new List<string>();
                while (reader.Read()) {
                    switch (reader.NodeType) {
                        case XmlNodeType.Element:
                            if (reader.Name == "Response")
                                xmlResponse = true;
                            if (reader.Name == "DetailLines")
                                xmlDetailLines = true;
                            if (reader.Name == "AuthorizationCode")
                                xmlAuthorizationCode = true;

                            break;
                        case XmlNodeType.Text: //Display the text in each element.
                            if (xmlResponse) {
                                switch (reader.Value) {
                                    case "Authorized":
                                        result.Authorization.Response = ACHeck21_Web_Services_Demo.com.acheck21.gateway.ResponseType.Authorized;
                                        break;
                                    case "Declined":
                                        result.Authorization.Response = ACHeck21_Web_Services_Demo.com.acheck21.gateway.ResponseType.Declined;
                                        break;
                                    case "Warning":
                                        result.Authorization.Response = ACHeck21_Web_Services_Demo.com.acheck21.gateway.ResponseType.Warning;
                                        break;
                                    default:
                                        break;
                                }
                            }
                            if (xmlAuthorizationCode)
                                result.Authorization.AuthorizationCode = reader.Value;
                            if (xmlDetailLines) {
                                detailLines.Add(reader.Value);
                            }

                            break;
                        case XmlNodeType.EndElement: //Display the end of the element.
                            if (reader.Name == "Response")
                                xmlResponse = false;
                            if (reader.Name == "DetailLines") {
                                xmlDetailLines = false;
                                result.Authorization.DetailLines = detailLines.ToArray();
                            }
                            if (reader.Name == "AuthorizationCode")
                                xmlAuthorizationCode = false;
                            break;
                    }
                }

            }

            return result;
        }

        public ACHeck21_Web_Services_Demo.com.acheck21.gateway.CreateCheckResult CreateCheck(string clientId, string clientTag, string payee, string checkNr, string routingNr, string dda, decimal amount, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass ecc, string frontFn, string rearFn, string micr, DateTime postDate, string[] addenda) {
            List<KeyValuePair<string, string>> headerVars = new List<KeyValuePair<string, string>>() { 
                new KeyValuePair<string, string>("ClientID", clientId),
                new KeyValuePair<string, string>("ClientTag", clientTag),
                new KeyValuePair<string, string>("IndividualName", payee),
                new KeyValuePair<string, string>("AccountType", "Checking"),
                new KeyValuePair<string, string>("EntryClass", ecc.ToString()),
                new KeyValuePair<string, string>("TransitNumber", routingNr),
                new KeyValuePair<string, string>("DDANumber", dda), 
                new KeyValuePair<string, string>("CheckNumber", checkNr),
                new KeyValuePair<string, string>("CheckAmount", amount.ToString("0.00")) };
            string query = GetVariableString(headerVars);

            string response = "";
            com.acheck21.gateway.GatewayServiceResult gsr = GetResponse("https://gateway.acheck21.com/GlobalGateway/REST/check", WebRequestMethods.Http.Post, query, out response);

            com.acheck21.gateway.CreateCheckResult result = new ACHeck21_Web_Services_Demo.com.acheck21.gateway.CreateCheckResult();
            result.Code = gsr.Code;
            result.Message = gsr.Message;

            return result;
        }

        public ACHeck21_Web_Services_Demo.com.acheck21.gateway.CreateCheckWithIRResult CreateCheckWithIR(string clientId, string payee, string checkNr, string routingNr, string dda, decimal amount, string frontFn, string rearFn, string micr, DateTime postDate) {
            List<KeyValuePair<string, string>> headerVars = new List<KeyValuePair<string, string>>() { 
                new KeyValuePair<string, string>("ClientID", clientId),
                new KeyValuePair<string, string>("IndividualName", payee),
                new KeyValuePair<string, string>("AccountType", "Checking"),
                new KeyValuePair<string, string>("TransitNumber", routingNr),
                new KeyValuePair<string, string>("DDANumber", dda), 
                new KeyValuePair<string, string>("CheckNumber", checkNr),
                new KeyValuePair<string, string>("CheckAmount", amount.ToString("0.00")) };
            string query = GetVariableString(headerVars);

            string response = "";
            com.acheck21.gateway.GatewayServiceResult gsr = GetResponse("https://gateway.acheck21.com/GlobalGateway/REST/check/IR", WebRequestMethods.Http.Post, query, out response);

            com.acheck21.gateway.CreateCheckWithIRResult result = new ACHeck21_Web_Services_Demo.com.acheck21.gateway.CreateCheckWithIRResult();
            result.Code = gsr.Code;
            result.Message = gsr.Message;

            return result;
        }

        public ACHeck21_Web_Services_Demo.com.acheck21.gateway.CreateValidatedCheckResult CreateValidatedCheck(string clientId, string clientTag, string payee, string checkNr, string routingNr, string dda, decimal amount, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass ecc, string frontFn, string rearFn, string micr, string dlNumber, string phone, DateTime postDate, string[] addenda) {
            throw new NotImplementedException();
        }

        public ACHeck21_Web_Services_Demo.com.acheck21.gateway.CreateValidatedCheckWithIRResult CreateValidatedCheckWithIR(string clientId, string payee, string checkNr, string routingNr, string dda, decimal amount, string frontFn, string rearFn, string micr, string dlNumber, string phone, DateTime postDate) {
            throw new NotImplementedException();
        }

        public ACHeck21_Web_Services_Demo.com.acheck21.gateway.FindChecksDetailsResult FindChecksDetails(string clientId, string query) {
            throw new NotImplementedException();
        }

        public ACHeck21_Web_Services_Demo.com.acheck21.gateway.FindReturnsDetailsResult FindReturnsDetails(string clientId, string query) {
            throw new NotImplementedException();
        }

        public ACHeck21_Web_Services_Demo.com.acheck21.gateway.FindUserClientsResult FindUserClients(string username) {
            throw new NotImplementedException();
        }

        public ACHeck21_Web_Services_Demo.com.acheck21.gateway.SendBatchResults SendBatch(string clientId, string filename) {
            throw new NotImplementedException();
        }

        public com.acheck21.gateway.CreateRCCResult CreateRCC(string clientId, string payee, string checkNr, string routingNr, string dda, decimal amount, DateTime checkDate, string payer, string addr1, string addr2, string addr3, string endorse1, string endorse2, string endorse3, string endorse4, string sigText) {
            throw new NotImplementedException();
        }

        #endregion
    }
}
