﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace ACHeck21_Web_Services_Demo {
    public partial class FormMain {

        private partial class GlobalGatewayHandler {
            private string m_ggFrontFn, m_ggRearFn;

            private List<KeyValuePair<string, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass>> m_eccList =
                new List<KeyValuePair<string, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass>>();

            public void InitializeLocalVariables() {
                m_frm.m_dtpTransTo.Value = DateTime.Today;
                m_frm.m_dtpTransFrom.Value = DateTime.Today;
                m_frm.m_dtpReturnsFrom.Value = DateTime.Today;
                m_frm.m_dtpReturnsTo.Value = DateTime.Today;

                m_eccList.Add(new KeyValuePair<string, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass>(
                    "C21 - Check 21 Image Cash Letter", ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass.C21));
                m_eccList.Add(new KeyValuePair<string, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass>(
                    "BOC - Back Office Conversion", ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass.BOC));
                m_eccList.Add(new KeyValuePair<string, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass>(
                    "ARC - Accounts Receivable Conversion", ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass.ARC));
                m_eccList.Add(new KeyValuePair<string, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass>(
                    "POP - Point-of-Purchase", ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass.POP));
                m_eccList.Add(new KeyValuePair<string, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass>(
                    "TEL - Telephone Initiated Payment", ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass.TEL));
                m_eccList.Add(new KeyValuePair<string, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass>(
                    "WEB - Web Initiated Payment", ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass.WEB));
                m_eccList.Add(new KeyValuePair<string, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass>(
                    "PPD - Pre-arranged Bill Payment / Credit", ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass.PPD));
                m_eccList.Add(new KeyValuePair<string, ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass>(
                    "CCD - Corporate Cash Disbursement", ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass.CCD));

                m_frm.m_cmbxEntryClass.DataSource = m_eccList;
                m_frm.m_cmbxEntryClass.DisplayMember = "Key";
                m_frm.m_cmbxEntryClass.ValueMember = "Value";

                m_frm.m_cmbxMethodTarget.DataSource = new List<string>() { "CreateCheck", "CreateCheckWithIR", "CreateValidatedCheck", 
                    "CreateValidatedCheckWithIR", "AuthorizeCheck", "CreateRCC" };
                m_frm.m_cmbxProtocol.DataSource = new List<string>() { "SOAP", "REST" };
            }

            public void BtnCreateCheck_Click(object sender, EventArgs e) {
                if (FormAuthenticate.GetInstance().IsConnected) {
                    IGlobalGateway gateway = ((string)m_frm.m_cmbxProtocol.SelectedItem == "SOAP") ?
                        new GlobalGatewaySOAP(FormAuthenticate.GetInstance().Username, FormAuthenticate.GetInstance().Password) as IGlobalGateway :
                        new GlobalGatewayREST(FormAuthenticate.GetInstance().Username, FormAuthenticate.GetInstance().Password) as IGlobalGateway;

                    com.acheck21.gateway.GatewayServiceResult result = new ACHeck21_Web_Services_Demo.com.acheck21.gateway.GatewayServiceResult();
                    StringBuilder str = new StringBuilder();
                    switch ((string)m_frm.m_cmbxMethodTarget.SelectedItem) {
                        case "AuthorizeCheck": {
                                m_frm.LogQueryStart("AuthorizeCheck", "");
                                com.acheck21.gateway.AuthorizeCheckResult response = gateway.AuthorizeCheck(m_frm.ParseClientId(m_frm.m_cbCheckFrom.Text), m_frm.m_tbMicrGG.Text.Trim(),
                                    m_frm.m_tbRoutingNrGG.Text.Trim(), m_frm.m_tbAccountNrGG.Text.Trim(), m_frm.m_tbCheckNrGG.Text.Trim(), Convert.ToDecimal(m_frm.m_tbAmountGG.Text.Trim()),
                                    "", "");
                                m_frm.LogQueryEnd("AuthorizeCheck");

                                result.Code = response.Code;
                                result.Message = response.Message;

                                switch (response.Authorization.Response) {
                                    case ACHeck21_Web_Services_Demo.com.acheck21.gateway.ResponseType.Authorized:
                                        str.AppendLine("Authorization Code: AUTHORIZED");
                                        break;
                                    case ACHeck21_Web_Services_Demo.com.acheck21.gateway.ResponseType.Declined:
                                        str.AppendLine("Authorization Code: DECLINED");
                                        break;
                                    case ACHeck21_Web_Services_Demo.com.acheck21.gateway.ResponseType.Warning:
                                        str.AppendLine("Authorization Code: WARNING");
                                        break;
                                    default:
                                        str.AppendLine("Authorization Code: ERROR");
                                        break;
                                }
                                str.AppendLine("Details:");
                                foreach (string line in response.Authorization.DetailLines) {
                                    str.AppendLine(line);
                                }
                                break;
                            }
                        case "CreateCheck": {
                                m_frm.LogQueryStart("CreateCheck", "");
                                com.acheck21.gateway.CreateCheckResult response = gateway.CreateCheck(m_frm.ParseClientId(m_frm.m_cbCheckFrom.Text), "", m_frm.m_tbPayTo.Text.Trim(),
                                    m_frm.m_tbCheckNrGG.Text.Trim(), m_frm.m_tbRoutingNrGG.Text.Trim(), m_frm.m_tbAccountNrGG.Text.Trim(), Convert.ToDecimal(m_frm.m_tbAmountGG.Text.Trim()),
                                    (ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass)m_frm.m_cmbxEntryClass.SelectedValue, m_ggFrontFn, m_ggRearFn, m_frm.m_tbMicrGG.Text,
                                    DateTime.Today, null);
                                m_frm.LogQueryEnd("CreateCheck");

                                result.Code = response.Code;
                                result.Message = response.Message;

                                if (response.Code == (int)Enums.ResponseCode.NO_ERROR)
                                    str.AppendLine("Transaction ID: " + response.CheckID);
                                break;
                            }
                        case "CreateCheckWithIR": {
                                m_frm.LogQueryStart("CreateCheckWithIR", "");
                                com.acheck21.gateway.CreateCheckWithIRResult response = gateway.CreateCheckWithIR(m_frm.ParseClientId(m_frm.m_cbCheckFrom.Text),
                                    m_frm.m_tbPayTo.Text.Trim(), m_frm.m_tbCheckNrGG.Text.Trim(), m_frm.m_tbRoutingNrGG.Text.Trim(), m_frm.m_tbAccountNrGG.Text.Trim(),
                                    Convert.ToDecimal(m_frm.m_tbAmountGG.Text.Trim()), m_ggFrontFn, m_ggRearFn, m_frm.m_tbMicrGG.Text.Trim(), DateTime.Today);
                                m_frm.LogQueryEnd("CreateCheckWithIR");

                                result.Code = response.Code;
                                result.Message = response.Message;

                                if (response.Code == (int)Enums.ResponseCode.NO_ERROR) {
                                    str.AppendLine("Transaction ID: ").Append(response.CheckID.ToString());
                                    str.AppendLine("Calculated EntryClass: ").Append(response.CheckInfo.EntryClass);
                                }
                                break;
                            }
                        case "CreateValidatedCheck": {
                                m_frm.LogQueryStart("CreateValidatedCheck", "");
                                com.acheck21.gateway.CreateValidatedCheckResult response = gateway.CreateValidatedCheck(m_frm.ParseClientId(m_frm.m_cbCheckFrom.Text), "", m_frm.m_tbPayTo.Text.Trim(),
                                    m_frm.m_tbCheckNrGG.Text.Trim(), m_frm.m_tbRoutingNrGG.Text.Trim(), m_frm.m_tbAccountNrGG.Text.Trim(), Convert.ToDecimal(m_frm.m_tbAmountGG.Text.Trim()),
                                    (ACHeck21_Web_Services_Demo.com.acheck21.gateway.EntryClass)m_frm.m_cmbxEntryClass.SelectedValue, m_ggFrontFn, m_ggRearFn, m_frm.m_tbMicrGG.Text,
                                    "", "", DateTime.Today, null);
                                m_frm.LogQueryEnd("CreateValidatedCheck");

                                result.Code = response.Code;
                                result.Message = response.Message;

                                if (response.Code == (int)Enums.ResponseCode.NO_ERROR) {
                                    str.AppendLine("Authorization Code: ").Append(response.ValidationData.AuthorizationCode);
                                    str.AppendLine("Details:");
                                    foreach (string line in response.ValidationData.DetailLines) {
                                        str.AppendLine(line);
                                    }
                                    str.AppendLine("Transaction ID: ").Append(response.CreateData.CheckID);
                                }
                                break;
                            }
                        case "CreateValidatedCheckWithIR": {
                                m_frm.LogQueryStart("CreateValidatedCheckWithIR", "");
                                com.acheck21.gateway.CreateValidatedCheckWithIRResult response = gateway.CreateValidatedCheckWithIR(m_frm.ParseClientId(m_frm.m_cbCheckFrom.Text), m_frm.m_tbPayTo.Text.Trim(),
                                    m_frm.m_tbCheckNrGG.Text.Trim(), m_frm.m_tbRoutingNrGG.Text.Trim(), m_frm.m_tbAccountNrGG.Text.Trim(), Convert.ToDecimal(m_frm.m_tbAmountGG.Text.Trim()),
                                    m_ggFrontFn, m_ggRearFn, m_frm.m_tbMicrGG.Text,
                                    "", "", DateTime.Today);
                                m_frm.LogQueryEnd("CreateValidatedCheckWithIR");

                                result.Code = response.Code;
                                result.Message = response.Message;

                                if (response.Code == (int)Enums.ResponseCode.NO_ERROR) {
                                    str.Append("Authorization Code: ").Append(response.ValidationData.AuthorizationCode).AppendLine();
                                    str.Append("Details:").AppendLine();
                                    foreach (string line in response.ValidationData.DetailLines) {
                                        str.Append(line).AppendLine();
                                    }

                                    str.Append("Transaction ID: ").Append(response.CheckInfo.CheckID).AppendLine();
                                    str.Append("Entry Class: ").Append(response.CheckInfo.EntryClass).AppendLine();
                                }
                                break;
                            }
                        case "CreateRCC": {
                                m_frm.LogQueryStart("CreateRCC", "");
                                com.acheck21.gateway.CreateRCCResult response = gateway.CreateRCC(m_frm.ParseClientId(m_frm.m_cbCheckFrom.Text), m_frm.m_tbPayTo.Text.Trim(),
                                    m_frm.m_tbCheckNrGG.Text.Trim(), m_frm.m_tbRoutingNrGG.Text.Trim(), m_frm.m_tbAccountNrGG.Text.Trim(), Convert.ToDecimal(m_frm.m_tbAmountGG.Text.Trim()),
                                    DateTime.Today, "Test Payor", "1111 Test Lane", "Lake Saint Louis, MO 63367", "", "", "", "", "", "SIGNATURE ON FILE");
                                m_frm.LogQueryEnd("CreateRCC");

                                result.Code = response.Code;
                                result.Message = response.Message;

                                if (response.Code == (int)Enums.ResponseCode.NO_ERROR)
                                    str.AppendLine("Transaction ID: " + response.CheckID);
                                break;
                            }
                        default:
                            break;
                    }

                    if (result.Code != (int)Enums.ResponseCode.NO_ERROR) {
                        MessageBox.Show("CreateCheckResult method error: " + result.Message + "", "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    } else {
                        MessageBox.Show(str.ToString());
                    }
                }
            }

            public void BtnBrowseFrontImageGG_Click(object sender, EventArgs e) {
                if (m_frm.m_openDlgImage.ShowDialog() == DialogResult.OK) {
                    m_ggFrontFn = m_frm.m_openDlgImage.FileName.Trim();
                    int size = ConvertToFile(m_ggFrontFn, "frontImage.jpg", 0, 50);
                    m_frm.m_pbFrontGG.Image = m_frm.GetImage("frontImage.jpg");
                }
            }

            public void BtnBrowseRearImageGG_Click(object sender, EventArgs e) {
                if (m_frm.m_openDlgImage.ShowDialog() == DialogResult.OK) {
                    m_ggRearFn = m_frm.m_openDlgImage.FileName.Trim();
                    int size = ConvertToFile(m_ggRearFn, "rearImage.jpg", 0, 50);
                    m_frm.m_pbRearGG.Image = m_frm.GetImage("rearImage.jpg");
                }
            }

            public void BtnGetTrans_Click(object sender, EventArgs e) {
                if (FormAuthenticate.GetInstance().IsConnected) {
                    List<IQueryOperation> queries = new List<IQueryOperation>();
                    queries.Add(new QueryBetween("UploadDate", m_frm.m_dtpTransFrom.Value.ToString("yyyy-MM-dd"), m_frm.m_dtpTransTo.Value.ToString("yyyy-MM-dd")));
                    string query = QueryGenerator.GenerateXML(queries);

                    m_frm.LogQueryStart("FindChecksDetails", query);

                    IGlobalGateway gateway = new GlobalGatewaySOAP(FormAuthenticate.GetInstance().Username, FormAuthenticate.GetInstance().Password);
                    com.acheck21.gateway.FindChecksDetailsResult result = gateway.FindChecksDetails(m_frm.ParseClientId(m_frm.m_cbClientsTrans.Text), query);

                    m_frm.LogQueryEnd("FindChecksDetails");

                    if (result.Code != (int)Enums.ResponseCode.NO_ERROR) {
                        MessageBox.Show("FindChecksDetails method error: " + result.Message, "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    } else {
                        m_frm.m_dgvTrans.DataSource = result.Checks;
                        m_frm.m_dgvTrans.Refresh();
                    }
                }
            }

            public void BtnGetReturns_Click(object sender, EventArgs e) {
                if (FormAuthenticate.GetInstance().IsConnected) {
                    List<IQueryOperation> queries = new List<IQueryOperation>();
                    queries.Add(new QueryBetween("ReturnDate", m_frm.m_dtpReturnsFrom.Value.ToShortDateString(), m_frm.m_dtpReturnsTo.Value.ToShortDateString()));
                    string query = QueryGenerator.GenerateXML(queries);

                    m_frm.LogQueryStart("FindReturnsDetails", query);

                    IGlobalGateway gateway = new GlobalGatewaySOAP(FormAuthenticate.GetInstance().Username, FormAuthenticate.GetInstance().Password);
                    com.acheck21.gateway.FindReturnsDetailsResult result = gateway.FindReturnsDetails(m_frm.ParseClientId(m_frm.m_cbClientsReturns.Text), query);

                    m_frm.LogQueryEnd("FindReturnsDetails");

                    if (result.Code != (int)Enums.ResponseCode.NO_ERROR) {
                        MessageBox.Show("FindReturnsDetails method error: " + result.Message, "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    } else {
                        m_frm.m_dgvReturns.DataSource = result.Checks;
                        m_frm.m_dgvReturns.Refresh();
                    }
                }
            }

            public void BtnSendBatch_Click(object sender, EventArgs e) {
                if (FormAuthenticate.GetInstance().IsConnected) {
                    m_frm.LogQueryStart("SendBatch", "");

                    IGlobalGateway gateway = new GlobalGatewaySOAP(FormAuthenticate.GetInstance().Username, FormAuthenticate.GetInstance().Password);
                    com.acheck21.gateway.SendBatchResults result = gateway.SendBatch(m_frm.ParseClientId(m_frm.m_cbCheckFrom.Text), m_frm.m_openDlgBatch.FileName);
                    
                    m_frm.LogQueryEnd("SendBatch");

                    if (result.Code != (int)Enums.ResponseCode.NO_ERROR) {
                        MessageBox.Show("SendBatch method error: " + result.Message, "Web Services Execute Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    } else {
                        MessageBox.Show(result.Message);
                    }
                }
            }
        }

    }
}
