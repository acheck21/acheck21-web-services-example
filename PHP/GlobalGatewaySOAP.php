<?php
        try {
            $options = array(
                'soap_version'=>SOAP_1_2,
                'exceptions'=>0,
                'trace'=>1,
                'cache_wsdl'=>WSDL_CACHE_NONE
            );
            $client = new SoapClient('https://gateway.acheck21.com/GlobalGateway/WebServices/Gateway/2.3/Service.asmx?WSDL', $options);

            $response = $client->__soapCall('GetClient', array(null,array('Username'=>'someone@company.com','Password'=>'MyPassword','ClientID'=>'9900000000')));
	    }
        catch (Exception $e) {
            echo "<h2>Exception Error!</h2>";
            echo $e->getMessage();
        }

        echo "Code: " . $response->GetClientResult->Code . "\n";
        echo "Message: " . $response->GetClientResult->Message . "\n";
        
        echo "\n\n";
        
        echo "Request :\n" . $client->__getLastRequest() . "\n\n";
        
        echo "Response:\n" . $client->__getLastResponse() . "\n\n";
        
        echo "Headers:\n" . $client->__getLastRequestHeaders() . "\n\n";
?>
