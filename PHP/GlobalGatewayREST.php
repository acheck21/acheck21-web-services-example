<?php 
 
 $request = "ClientID=9900000000" . 
            "&ClientTag=xxxxxxxxx" . 
            "&IndividualName=TestUser" . 
            "&CheckNumber=12345" . 
            "&TransitNumber=112000066" . 
            "&DDANumber=12345" . 
            "&AccountType=Checking" . 
            "&CheckAmount=55" . 
            "&EntryClass=WEB";

   $url = "https://gateway.acheck21.com/GlobalGateway/REST/check";

   $ch = curl_init();

   curl_setopt( $ch, CURLOPT_URL, $url);
   curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 30 );
   curl_setopt( $ch, CURLOPT_TIMEOUT, 30 );
   curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)" );
   curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
   curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
   curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
   curl_setopt( $ch, CURLOPT_POST, 1 );
   curl_setopt( $ch, CURLOPT_VERBOSE, 1 );
   curl_setopt( $ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
   curl_setopt( $ch, CURLOPT_USERPWD, "test:password");
   curl_setopt( $ch, CURLOPT_POSTFIELDS, $request );
   curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

   $process_result = curl_exec( $ch );
?>